\documentclass[twocolumn,letterpaper,12pt,fleqn]{article}

\usepackage[margin=0.5in]{geometry}
\usepackage{amsmath}

\usepackage{caption}
\usepackage{subcaption}
\usepackage{graphicx}

\begin{document}


\title{2D/3D Dynamic Arm Camera Calibration}
\author{Dennis Shtatnov \\ dennis@drexel.edu}
\date{}
\maketitle


%\begin{abstract}
%Your abstract goes here...
%...
%\end{abstract}


\section{Introduction}

	Creating a robot to sample an environment autonomously is a well studied problem. Most existing approaches utilize sensors (such as a Kinect) fixed relative to the robot's body to perform SLAM. Occasionally though, it is also desirable to have an addition dynamic camera on a robotic arm or other moving part of the robot for performing more precise observations of the environment. This report details a procedure for synchronizing a dynamic arm-mounted camera with fixed depth/color sensors.

	\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{imgs/setup}
	\caption{The multi-camera setup used for testing.}
	\label{fig:setup}
	\end{figure}



	This approach depends on a set of fixed reliable depth/color sensors and a moving color camera. For our experiments, a Kinect 2 was used as the static sensors and a Basler acA2040-90uc was used as the dynamic camera mounted on a robotic arm. The robotic arm being used is a Robai Cyton Gamma 300R2. A high accuracy and repeatibility arm is required for this procedure. Additionally we assume that the dynamic camera is observing a scene that is always at least partially in the field of view of the depth sensor. But, the dynamic camera itself does not need to be visible by the depth sensor.


%	Although the arm has repeatablity, the position reported by the arm's servos can not necessarily be used to directly derive the position of the camera. The camera may be offset or rotated from the center axis of the arm by unknown amounts. Also, the arm used in our experiments had noticeable sway so accuracy is not guranteed by repeatability. We solve these issues by measuring the true pose of the dynamic camera independently of the servo positions and then creating correlations between reported servo positions and the measured true pose.


\section{Camera Model}



	We would like to know the pose of the dynamic camera \textit{in the reference frame of the static sensors} at all points of time. More precisely, we are trying to solve for all variables of the 3D to 2D point projection problem (\ref{eq:bigproj},\ref{eq:proj},\ref{radial}) shown below for all points $p'$ in the image:

	\begin{equation} \label{eq:bigproj}
		s
		\begin{bmatrix} u \\ v \\ 1 \end{bmatrix}
		=
		\begin{bmatrix}
			f_x & 0 & c_x \\
			0 & f_y & c_y \\
			0 & 0 & 1
		\end{bmatrix}
		\begin{bmatrix}
			r_{11} & r_{11} & r_{11} & t_1 \\
			r_{21} & r_{22} & r_{23} & t_2 \\
			r_{31} & r_{32} & r_{33} & t_3
		\end{bmatrix}
		\begin{bmatrix}
		X \\ Y \\ Z \\ 1
		\end{bmatrix}
	\end{equation}

	this equation will be referenced by the following abbreviated form:

	\begin{equation} \label{eq:proj}
		p_{2d} = M \left[ R | t \right] p_{3d}
	\end{equation}

	\begin{equation} \label{radial}
		p_{2d}' = p_{2d}\left(1+k_1 r^2 + k_2 r^4 + \ldots  \right)
	\end{equation}

	where each variable represents the following

	\begin{itemize}
		\item $p_{2d}'$ is the location of the final pixel in the distorted image
		\item $p_{2d}$ is the homogenous 2d location $(u,v)$ in the dynamic camera's image
		\item $p_{3d}$ is the original 3D coordinates in the reference frame that were projected to $p'$
		\item $M$ is the camera matrix for the specific physical camera that is used. It is independent of the position of the camera and only consists of the focal lengths $f_x$ and $f_y$ and the optical centers $c_x$ and $c_y$.
		\item $k$ is a vector of distortion coefficients representing the degree of radial distortion created by the lens

		\item $R$ is the rotation matrix between the orientation of the reference frame and the dynamic camera. Likewise, $t$ is the translation between the same two frames.


	\end{itemize}

	We will refer to $M$ and $k$ as the instrinsic parameters of the camera as they are internally common to all images taken by the camera, and $\left[R|t\right]$ will be refered to as the extrinsic parameters are they are location dependent.

	When a 3d object point is projected onto the image plane, it's 2d point is determined by equation \ref{eq:proj}. Then the point is distorted by the lens as described by equation \ref{radial}. The final distorted point is what is saved as the image.


	The general procedure we will follow is to reverse this process. In section \ref{sec:instrinsic} we will describe how to compute camera constant values and reverse the distortion. Then, the camera will be used at several different positions in section \ref{sec:pnp} to extract the pose of the camera for a large range of inputs. Section \ref{sec:handeye} describes a model for relating the different reference frames of the system and how to solve for all relationships between them. Finally, section \ref{sec:operation} describes how to use these precomputed poses to estimate the pose of the dynamic camera during operation. For most of these steps, we heavily use functions built in to OpenCV which are denoted in this paper as \textit{cv2::functionname()} and PCL which is denoted by \textit{pcl::functionname()}




\section{Instrinsic Calibration} \label{sec:instrinsic}
% Calibrate camera

	The first step in the process is to determine the unchanging parameters of every camera. This includes the camera matrix $M$ and the distortion coefficients $k$. Starting with one of the cameras, we take pictures of a chessboard pattern of known size and then we use \textit{cv2::findChessboardCorners()} to locate the corners of the pattern. We repeat this for the pattern in several different orientations and positions in the image. Using the assumption that the chessboard is planar, we can say that the corresponding 3d object points of the 2d chessboard points are on a uniform grid at $z=0$ with $x$ and $y$ incremented by a uniform value for all internal points of the chessboard. Using \textit{cv2::calibrateCamera()}, we can provide just the set of 2d image points and their corresponding 3d grid coordinates to estimate the camera matrix and the distortion coefficients for the camera. In our setup, this needs to be repeated for the Kinect's color camera, it's IR/depth camera, and the dynamic arm camera.

	For simplifying later computations we also calibrate both of the Kinect's cameras together by taking synchonized pictures of the same chessboard and use \textit{cv2::calibrateStereo()} to determine the fixed extrinsic translation and rotation between the two cameras. The images are then undistorted and this tranlation and rotation is used to transform the color image to the IR image. Finally, the entire depth/color system is represented as one PCL XYZ-RGB point cloud for later processing. If using the Kinect 2, there always exists a publically available ROS package called \textit{iai\_kinect2} that will handle the calibration and point cloud conversion.


	\begin{figure}[h]
			\centering
			\begin{subfigure}[b]{0.45\linewidth}
					\includegraphics[width=\linewidth]{imgs/goproRaw}
					\caption{Raw Image}
					\label{fig:goproRaw}
			\end{subfigure}%
			~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc.
			  %(or a blank line to force the subfigure onto a new line)
			\begin{subfigure}[b]{0.45\linewidth}
					\includegraphics[width=\linewidth]{imgs/goproUndist}
					\caption{Undistorted Image}
					\label{fig:goproUndist}
			\end{subfigure}

			\caption{Sample undistortion using \textit{cv2::undistort()} after instrinsic calibration. Note the straightened geometry. }\label{fig:distortion}
	\end{figure}


	\begin{figure}[h]
			\centering
			\begin{subfigure}[b]{0.45\linewidth}
					\includegraphics[width=\linewidth]{imgs/kinectBad}
					\caption{Raw Point Cloud}
					\label{fig:kinectBad}
			\end{subfigure}%
			~
			\begin{subfigure}[b]{0.45\linewidth}
					\includegraphics[width=\linewidth]{imgs/kinectGood}
					\caption{Calibrated Cloud}
					\label{fig:kinectGood}
			\end{subfigure}

			\caption{Comparison of uncalibrated vs. calibrated point clouds from different orientations. Note the disappearance of wall colored edges around the checkerboard pattern.}\label{fig:distortion}
	\end{figure}




\section{Sampling} \label{sec:sampling}
% What to collect and how to collect it

	The calibration data consists of a set of point clouds, images, and transformations recorded during operation of the arm. First, a planar calibration pattern such as a chessboard is placed in front of the robot. For improved accuracy, it is highly recommended to use a pattern consisting of 2 or 3 orthogonal patterns. Next, the arm must traverse a preset path. In this paper the translation of the path is similar to a scanline grid pattern. For the calibration to be correctly estimated, the orientation at each point of the path must be calculated to maintain view of the pattern and the orientation must change in at least 2 DOF over the entire path. Otherwise, the camera to gripper offset will not be estimatable.

	The robotic arm's software interface should be setup to take a 4x4 transformation matrix as an argument and instruct the underlying hardware interface to move the arm to the corresponding rotation and translation. For the Cyton arm used here, the final end effector must be set to be of type \textit{FrameEndEffector} for full control of all DOF. Once the command is complete, the software should read the physical servo angles. After transforming the angles by the forward kinematics relations of the arm, a feedback matrix $B_i$ should be returned holding the closest estimation of the physical system. Note that some arm drivers may return the last commanded position rather than querying the servos, so it is important to ensure that these values are correctly obtained from the hardware.

	Another observation for arms mounted as in figure \ref{fig:setup} is that the coordinate axes of the arm and Kinect do not correspond. For example, the up vertical direction for the Kinect is $-y$ while the arm represents it as $+z$. This may result in extremely large or incorrect solutions in the calibration steps presented later in this paper. A useful optimization that should be integrated into the arm's software is a conversion of the commanded position from the world coordinate frame to the arm frame. Likewise, it should invert this conversion when providing feedback positions. The following matrix was used in our testing for doing arm to world transformations:

	\begin{equation}
		A_i =
		\begin{bmatrix}
			1 & 0 & 0 & 0 \\
			0 & 0 & -1 & 0 \\
			0 & 1 & 0 & 0 \\
			0 & 0 & 0 & 1
		\end{bmatrix} A_{i,raw}
	\end{equation}

	At point $i$ along the path, we will record the arm's position as a 4 $\times$ 4 matrix $B_i$, an RGB point cloud from the static camera $C_i$, and a single image $D_i$ from the dynamic camera. The sample that needs to be recorded is the tuple $(B_i\;, C_i\;, D_i)$





%	Using $n$ calibrated samples we will derive two offset values show in \ref{eq:toffset} and \ref{eq:roffset} that will be used to model the arm's motion. In these equations $q(x)$ is a transformation from rotation matrices to quaternions and $q^{-1}(x)$ is the reverse operation (both normalize after/before conversions respectively).

%	\begin{equation} \label{eq:toffset}
%		\Delta t = \frac{\sum_{i=0}^{n} (t_i - \alpha_i) }{ n }
%	\end{equation}

%	\begin{equation} \label{eq:roffset}
%		\Delta R =
%		q^{-1}\left(
%			\frac
%				{\sum_{i=0}^n q\left( \theta_i^{-1} R_i \right)}
%				{n}
%		\right)
%	\end{equation}

%	These offset values are simply the average discrepency between the arm's instructed pose and the reference measurement separated into a rotation and translation term.


%	Now we formulate the equations for an unknown image $i$. For converting a static reference pose \( \left( R_{i}, t_{i} \right) \) to the pose \( \left(  \alpha_i, \theta_i \right) \) of the arm in the dynamic reference frame, we will apply a linear transformation based on the calibrated data as shown in equations \ref{eq:trans} and \ref{eq:rot}. During operation of the robot, these equations will be used to create instructions for the arm to go to a specific point in the reference space or vice versa.

%	\begin{equation} \label{eq:trans}
%		t_i = \alpha_i + \Delta t
%	\end{equation}

%	\begin{equation} \label{eq:rot}
%		R_i = \Delta R \; \theta_i
%	\end{equation}



\section{Planar Back-Projection} \label{sec:planes}
% RANSAC plane estimation and back-project rays onto the planes

	Each of the calibration patterns is assumed to be planar. To represent the planes, we will use the general form below:
	\begin{equation} \label{eq:plane_general}
		ax + by + cy + d = 0
	\end{equation}

	Let $h = \sqrt{a^2 + b^2 + c^2}$, then the normal vector and a point on the plane are defined as:

	\begin{equation}
		N = \begin{bmatrix}
			a / h \\ b / h \\ c / h
		\end{bmatrix}
	\end{equation}

	\begin{equation}
		P = -\frac{d}{h} N
	\end{equation}

	To extract a plane from a point cloud, we use a \textit{pcl::SACSegmentation} filter using a RANSAC plane model. This filter returns the model coefficients $a$, $b$, $c$, and $d$.

	By the standard pin hole camera model described in equation \ref{eq:bigproj}, each pixel in an image $i$ can be modeled by a ray of the form given in equation \ref{eq:ray} parametrized by $k$ that intersects with a 3d point in the scene. In this equation, $p$ is the location of a 2d pixel in the image. Note the $c_x$, $c_y$, $p$, and $f$ are all in pixel units. Also, $R$ and $t$ are the rotation matrix and the translation vector of the camera's image with respect to the world origin.

	\begin{equation} \label{eq:rayslope}
		s = R^{-1}
			\begin{bmatrix} (p_x - c_x) / f_x \\ (p_y - c_y) / f_y \\ 1 \end{bmatrix}
	\end{equation}

	\begin{equation} \label{eq:ray}
		r\left(k\right) = t + k s ,\;k \geq 0
	\end{equation}



%	Assuming the scene is planar with no occlusion, we can project all four corners of the dynamic camera's image onto the scene using algebraic plane intersection. Using the fitted plane model from section \ref{sec:extrinsic}, the scene plane can be represented by the standard plane form in equation \ref{eq:plane}. Combining this with the ray equation we can derive the value of the free parameter \(k\) at the intersection point as shown in \ref{eq:planeinter}.


	Equation \ref{eq:plane_general} can be rewritten in terms of dot product for a point $p = <x, y, z>$:

	\begin{equation} \label{eq:plane}
		(p - P) \cdot N = 0
	\end{equation}


	By solving this with the equation \ref{eq:ray}, the value of $k$ can be found for any plane/pixel ray intersection:

	\begin{equation} \label{eq:planeinter}
		k = \frac{ \left( P - t \right) \cdot N }{ s \cdot N }
	\end{equation}


%	For an arbitrary scene, voxel based algorithms such as \textit{pcl::octree::OctreePointCloudSearch}'s function \(getIntersectedVoxelIndices() \), can also be used to find the corresponding scene points to the dynamic image.


%	Using the four corner intersections, we solve for a homography matrix to map the static image to the dynamic image. Once the images of warped to the same viewpoint, we can perform further feature matching to refine the initial estimates of \(R\) and \(t\)

%	\begin{figure}[h]
%		\centering
%		\includegraphics[width=\linewidth]{imgs/visview}
%		\caption{Visualization of ray casting of image corners in the 3d frame. The sphere represents the estimated translation of the camera and the rays are the projections of the corners transformed by the estimated camera rotation.}
%		\label{fig:rayview}
%	\end{figure}


%	\begin{figure}[h]
%			\centering
%			\begin{subfigure}[b]{0.45\linewidth}
%					\includegraphics[width=\linewidth]{imgs/visview-cam}
%					\caption{Image projected in figure \ref{fig:rayview}.}
%					\label{fig:rayviewcam}
%			\end{subfigure}%
%			~
%			\begin{subfigure}[b]{0.45\linewidth}
%					\includegraphics[width=\linewidth]{imgs/casted}
%					\caption{Approximate ray casted image}
%					\label{fig:rayviewcasted}
%			\end{subfigure}

%			\caption{ Original dynamic image compared to approximate view reconstruction using static point cloud. }
%	\end{figure}


\section{Pose Estimation} \label{sec:pnp}
% PNP


	For each sample $(B_i\;, C_i\;, D_i)$, we will estimate another 4 $\times$
4 matrix $A_i$ that represents the position of the dynamic camera (represented by $D_i$) in the world coordinate system (represented by $C_i$).

	For each pair $(C_i, D_i)$, we find the 2D indices of the chessboard in the RGB component of $C_i$ and in $D_i$ resulting in the indice sets $(c_i^{2d}, d_i^{2d})$.


	We then use $c_i^{2d}$ to index the point cloud and retrieve the corresponding set of 3d coordinates $c_i^{3d}$. To improve the accuracy of the point cloud, we use a \textit{pcl::PassThrough} filter to remove all parts of the point cloud outside of a $0.05m$ margin around the bounding cube of the points $c_i^{3d}$. Then we approximate a plane model as described in section \ref{sec:planes}. Finally, using equation \ref{eq:planeinter}, $c_i^{3d}$ is regenerated using the filtered plane as the 3d scene and $c_i^{2d}$ as the camera rays. Assuming the point cloud is aligned with its RGB component, $R_i$ in equation \ref{eq:planeinter} can be ignored.

	In the case of a multiplanar calibration pattern, the above steps should be repeated for all patterns until all pattern points are in their appropriate sets.


	%The calibration samples consist of a point cloud and an image taken from the dynamic camera at a known arm position. The calibration samples are taken as follows: We place a chessboard in front of the robot and begin to incrementally move the robotic arm along a uniform path such as a grid pattern. At each increment  $i$, the arm is commanded to go to position $\alpha_i$ with orientation $\theta_i$. In our experiments, we keep $\theta_i$ constantly level with the z-axis between increments to simplify calibration. We capture a point cloud $C_i$ from the static sensors and a single image $D_i$ from the dynamic camera.


	We now have 3d to 2d correspondences in $(c_i^{3d}, d_i^{2d})$. By running \textit{cv2::solvePnP()} configured for an iterative estimation, we derive the rotation $R_i$ and the translation $t_i$ for each dynamic image using the correspondences and the intrinsic parameters of the dynamic camera found earlier. The last few steps are very similar to the instrinsic calibration procedure, except that due to the fact that we are using the depth sensor's 3d points rather than assuming a uniform grid at the origin, the resulting extrinsic parameters are now in the correct reference frame. Finally we combine $R_i$ and $t_i$ into $A_i$. Note that if the recommended OpenCV method is used, then $A_i$ should be replaced by $A_i^{-1}$.




\section{Hand-Eye Calibration} \label{sec:handeye}
% SVD solution of rotations
% Least squares solution of translation

	\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{imgs/system}
	\caption{Reference frames in the system. The world/origin is defined at the origin of the static depth camera. Additionally, coordinates will be defined with respect to the base of the arm, the end gripper of the arm and at the optical center of the dynamic camera.}
	\label{fig:system}
	\end{figure}

	The entire system can be viewed as a set of four homogenous matrix transformations at a time step $i$. The following correspond to their representation in figure \ref{fig:system}:

	\begin{itemize}
		\item $A_i$ is from the world to the optical center of the dynamic camera. This was estimated by the pose estimation process in section \ref{sec:pnp}.

		\item $B_i$ is from the base of the robotic arm to the final joint/gripper of the arm from the forward kinematics approach described in section \ref{sec:sampling}.

		\item $X$ is from the camera to the gripper.
		\item $Y$ is from the world to the base of the arm.
	\end{itemize}


	By the symmetry of these transformations we can see that the following matrix equation holds. To model this system, we must solve for the unknowns $X$ and $Y$.
	\begin{equation} \label{eq:axyb}
		A_i X = Y B_i
	\end{equation}

	Two solutions to the problem are presented below:

	\subsection{Simultaneous X and Y Solution}

		This solution to this equation is adapted from the one presented in \cite{zhuang}.

		\subsubsection{Quaternion Rotational Relation}

		Let $R_A$, $R_X$, $R_Y$, and $R_B$ be the 3x3 rotation matrices of $A$, $X$, $Y$, and $B$, and let $p_A$, $p_X$, $p_Y$, and $p_B$ be the respective 3x1 translation vectors. Equation \ref{eq:axyb} can be decomposed into a rotation equation \ref{eq:axyb_rot} and a translation equation \ref{eq:axyb_tran}.
		\begin{equation} \label{eq:axyb_rot}
			R_A R_X = R_Y R_B
		\end{equation}
		\begin{equation} \label{eq:axyb_tran}
			R_{A_i} p_X + p_{A_i} = R_Y p_{B_i} + p_Y
		\end{equation}


		Equation \ref{eq:axyb_rot} can be written as a quaternion product:

		\begin{equation} \label{eq:axyb_quatrot}
			\textbf{a} \circ \textbf{x} = \textbf{y} \circ \textbf{b}
		\end{equation}


		We will split each quaternion into a scalar and vector part such that $\textbf{a}=(a_0, a)$, $\textbf{x}=(x_0, x)$, $\textbf{y}=(y_0, y)$, $\textbf{b}=(b_0, b)$. We will also define $\Omega(v)$ to be the skew-symmetric matrix corresponding to the vector $v$.

		The quaternion equation can be solved by considering the scalar and vector parts of the equation separetely. After expansion of that solution, $G$, $w$ and $c$ can be defined representing the coefficients, unknowns, and remainder of the solution:

		\begin{equation} \label{eq:axyb_g}
			G \equiv
			\left[
				a_0 I + \Omega(a) + {a a^\top \over a_0}
				, \;
				-b_0 I + \Omega(b) - {a b^\top \over a_0}
			\right]
		\end{equation}

		\begin{equation}
			w \equiv \begin{bmatrix}
				x^\top / y_0 \\  y^\top / y_0
			\end{bmatrix}
		\end{equation}

		\begin{equation} \label{eq:axyb_c}
			c \equiv b - \left( b_0 / a_0 \right) a
		\end{equation}

		These were combined into the following relation equivalent to \ref{eq:axyb_quatrot} for normal rotations (\cite{zhuang} should be reviewed for cases until which this assumption fails). Note that \ref{eq:axyb_g} and \ref{eq:axyb_c} are dependent on the time step $i$, so they must be calculated for each sample.

		\begin{equation}
			\underbrace{G_i}_{3 \times 6}
			\underbrace{w}_{6 \times 1}
			=
			\underbrace{c_i}_{3 \times 1}
		\end{equation}



		Given $n \geq 3$ sample points, equation \ref{eq:handeye_last} can be solved using an SVD and the quaternions $\textbf{x}$ and $\textbf{y}$ can be recovered from $w$ and the assumption that the quaternions are normalized.
		\begin{equation} \label{eq:handeye_last}
			\begin{bmatrix}
				G_1 \\ G_2 \\ \vdots \\ G_n
			\end{bmatrix}
			w
			=
			\begin{bmatrix}
				c_1 \\ c_2 \\ \vdots \\ c_n
			\end{bmatrix}
		\end{equation}


		\subsubsection{Least Squares Translation}

		Given the rotational components, the translational equation \ref{eq:axyb_tran} can be rearranged to the following form with unknowns on the left hand side:

		\begin{equation} \label{eq:axyb_tran_new}
			R_{A_i} p_X - p_Y = R_Y p_{B_i} - p_{A_i}
		\end{equation}


		Using all $n$ samples, the translation in equation \ref{eq:axyb_tran_new} can be represented by the following form which can be solved by a least squares optimization to get the unknown translations:


		\begin{equation}
			\underbrace{
				\begin{bmatrix}
					R_{A_1} & -I_3 \\
					R_{A_2} & -I_3 \\
					\vdots & \vdots \\
					R_{A_n} & -I_3
				\end{bmatrix}
			}_{3n \times 6}
			%
			\underbrace{
				\begin{bmatrix}
					p_X \\
					p_Y
				\end{bmatrix}
			}_{6 \times 1}
			=
			\underbrace{
				\begin{bmatrix}
					R_Y p_{B_1} - p_{A_1} \\
					R_Y p_{B_2} - p_{A_2} \\
					\vdots \\
					R_Y p_{B_n} - p_{A_n}
				\end{bmatrix}
			}_{3n \times 6}
		\end{equation}

	\subsection{Approximate Solution}

		For systems where the dynamic camera is mounted very close to the start of the gripper, $p_X$ can be too small to estimate due to errors in prior steps in this estimation or the arm may not be capable of large enough rotations. In order to use the arm, a solution must still be found that is `good enough`. We will assume that $p_X$ is zero. Using only the translational components, we will adapt \cite{arun} to estimate $Y$.

		\subsubsection{Finding Y}

		As input to this first step, all the measurements of $p_{A_i}$ and $p_{B_i}$ are used as the input point sets. The centroid $c_s$ of each set $s$ of $n$ points is computed by the following relation:
		\begin{equation}
			c_s = \frac{1}{n} \sum_{i=1}^n s_i
		\end{equation}

		Using the centroids, the point sets are centered and placed in a covariance matrix to minimize error:

		\begin{equation}
			H = \sum_{i=1}^n
				\left( p_{A_i} - c_{p_A} \right)
				\left( p_{B_i} - c_{p_B} \right)^\top
		\end{equation}

		The optimal rotation $R$ is found by SVD:
		\begin{equation}
			SVD(H) = U, S, V
		\end{equation}
		\begin{equation}
			R = V U^\top
		\end{equation}

		If $det(R) = +1$, then $R_Y = R$, otherwise if $det(R) = -1$, then $V^` = V$ but $V_{i3}^` = -V_{i3} $ and $R_Y = V^` U^\top $

		The translational part $p_Y$ is now defined by the following closed form:
		\begin{equation}
			p_Y = -R_Y \; c_{p_A} + c_{p_B}
		\end{equation}




		\subsubsection{Finding X}

		Finally an $X$ consisting of solely rotation must be formed to align the orientations. Let $q(x)$ be the conversion from 3x3 rotations to quaternions, and let $m(x)$ be the reverse conversion. Both normalize the output/input quaternions respectively. At this point, $R_X$ can be trivially computed for each data point by evaluating \ref{eq:axyb_rot}. To optimize over the entire set of data $R_X$ will be estimated as the centroid of many independent calculated values.

		\begin{equation}
			R_X = m\left(
				\frac{1}{n}
				\sum_{i=1}^{n}
					q(R_{A_i}^{-1} R_Y R_{B_i})
			\right)
		\end{equation}








\section{Online Operation} \label{sec:operation}

	During real sampling of the environment, the lack of a calibration pattern inverts the problem to one where $A_i$ or $B_i$ are unknown and the calibrated values of $X$ and $Y$ must be used to solve for the entire system model.

	The typical use case would be moving the camera to a desired position in world coordinates. In this problem we need to find a $B_i$ for a given $A_i$. By rearranging equation \ref{eq:axyb}, we get the following unique solution:

	\begin{equation} \label{eq:operate_forward}
		B_i = Y^{-1} A_i X
	\end{equation}

	Following the completion of the move command, it is also useful to use the servo position feedback to invert the solution to check for error. Similarly to the previous method, another equation can be formed for doing this:
	\begin{equation} \label{eq:operate_backward}
		A_i = Y B_i X^{-1}
	\end{equation}





%	During real sampling of the environment, the calibrated translation and rotation offsets of the arm will be use to generate sampling paths based on the 3d geometry. Once a dynamic camera image is taken, we will already have the approximate pose of the camera based on the construction of the path.

%	Based on experimental tests, this approximation will likely be off by a few centimeters and several degrees. Using the 3d geometry of the scene, we can formulate a simple method for refining the estimate in post processing. By generating a homography from the static image to the dynamic image through ray casting, we can warp and downsample the images to create images of the same scene from the same orientation. Assumming a correct approximation of the pose, the cross-correlation of the two images such be a maximum. We can verify this be formulated the optimization as an iterative search based on adjustments of \(R\) and \(t\)

%	For detailed environments, a simple improvement on this technique would be to perform traditional feature matching on both color images using SIFT or another feature descriptor to get correspondences between 2d and 3d similar to what was done in section \ref{sec:extrinsic}. Then, the rotation and translation can be re-solved using the new live correspondences and the original pose estimation as an initial guess.


%	During real sampling of the environment, the chessboard will not be available to perform exact calibration. Rather, the previously stored tuples are used to train a k-Nearest-Neighbor (kNN) model where the input is $(\alpha_i, \theta_i)$ and the output is $(R_i, t_i)$. The system will query this model with the reported position of the arm at regular increments along the path of motion.

%	One big limitation of this model is that only positions close to already calibrated positions can be accurately estimated. Therefore it is essential to perform extrinsic sampling on all paths that the arm will use.






\section{Results}


	%The described procedure was tested on a rectangular grid path consisting of multiple vertical arm scan lines. The path along which the arm was instructed to move is shown in figure \ref{fig:targett}.

%	\begin{figure}[h]
%		\centering
%		\includegraphics[width=\linewidth]{imgs/targett}
%		\caption{Given zero error in the arm's movements and in the calibration process, we expect to extract this grid pattern as the translation of the image points.}
%		\label{fig:targett}
%	\end{figure}

	Figures \ref{fig:gridcalib} and \ref{fig:gridcubecalib} show sample images taken by the camera along the path. We compared both a standard chessuboard pattern (tilted away from the camera by ~30 degrees) and a multiplane calibration pattern and found that a multiplane calibration setup is optimal. For best results, the calibration pattern must be positioned as close as possible to both the 3d sensor and the dynamic camera while not being too close to be out of the field of view of any camera during the test. In your testing, the Kinect 2's color camera had issues with accurately detecting small far away chessboards that appeared large in the dynamic camera's view due to varying focal lengths and fields of view between the cameras. For instances like this, it may be more accurate to move the grid along which the arm moves to be farther back that z position of the 3d sensor.

	Live operation of poses remains to be tested.


	%The measured positions compared to the expected grid pattern is shown in \ref{fig:gptop} and \ref{fig:gpside}. The results show a reasonable conformation to the expected position. The most likely causes of error were a misalignment between the Kinect and arm resulting in skewed lines or excess swaying of the arm before the picture was taken.

	\begin{figure}[h]
			\centering
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel/1}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel/2}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel/3}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel/4}
			\end{subfigure}

			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel/5}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel/6}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel/7}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel/8}
			\end{subfigure}

			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel/9}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel/10}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel/11}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel/12}
			\end{subfigure}

			\caption{Images taken from the dynamic camera during grid path calibration. }\label{fig:gridcalib}
	\end{figure}

	\begin{figure}[h]
			\centering
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel_cube/1}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel_cube/2}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel_cube/3}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel_cube/4}
			\end{subfigure}

			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel_cube/5}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel_cube/6}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel_cube/7}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel_cube/8}
			\end{subfigure}

			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel_cube/9}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel_cube/10}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel_cube/11}
			\end{subfigure}%
			\begin{subfigure}[b]{0.25\linewidth}
					\includegraphics[width=\linewidth]{imgs/reel_cube/12}
			\end{subfigure}

			\caption{Images taken from the dynamic camera during grid path calibration using a multi-plane calibration object. }\label{fig:gridcubecalib}
	\end{figure}

	Using the a set of a hundred such images and a common point cloud of the calibration scene, we first extracted the pattern points in 3d as seem in figure \ref{fig:cubepoint}. Next we solve for each images pose as seen in figures \ref{fig:cubetrans}, \ref{fig:cubetransside}, and \ref{fig:rotation}.

	For hand eye calibration, the approximate method produced the best results as the quaternion method requires more error refinement prior to use. Additionally, the path along which the arm traverses needs to be modified to rotate the camera more and to utilize more of the arm's reach space.

	%The arm appears to be reasonably accurate within a few centimeters of the instructed position and has a fairly obvious downward orientation with respect to the 3d sensor. By averaging these data sets, we get an approximate offset of the center point of the grid pattern and the rotation between z axes of the dynamic cameras frame and the reference frame.


	\begin{figure}[h]
		\centering
		\includegraphics[width=\linewidth]{imgs/pattern3d}
		\caption{Cube pattern points extracted from the 3d point cloud.}
		\label{fig:cubepoint}
	\end{figure}

	\begin{figure}[h]
		\centering
		\includegraphics[width=\linewidth]{imgs/baslerCalibNew}
		\caption{Estimated translation of dynamic camera following a 0.2m by 0.1m grid.}
		\label{fig:cubetrans}
	\end{figure}

	\begin{figure}[h]
		\centering
		\includegraphics[width=\linewidth]{imgs/baslerCalibNewSide}
		\caption{Side view of figure \ref{fig:cubetrans}.}
		\label{fig:cubetransside}
	\end{figure}

	\begin{figure}[h]
		\centering
		\includegraphics[width=\linewidth]{imgs/rotation}
		\caption{Estimated orientation of dynamic camera compared to level z axis in reference frame.}
		\label{fig:rotation}
	\end{figure}


	After hand eye calibration, the results can be visually verified by applying $X$ and $Y$ to the points in order to align the coordinate systems. Figure \ref{fig:align} shows the translational agreement of the data after calibration.

	\begin{figure}[h]
		\centering
		\includegraphics[width=\linewidth]{imgs/alignment}
		\caption{Camera points and arm points transformed to the same coordinate system.}
		\label{fig:align}
	\end{figure}

	\begin{figure}[h]
		\centering
		\includegraphics[width=\linewidth]{imgs/align_rot}
		\caption{Camera and arm axis orientation alignment using rotation centroid method.}
		\label{fig:align_rot}
	\end{figure}




	%Figure \ref{fig:closertrans} shows another iteration of the test that was performed with the calibration pattern closer to the cameras which produced more linear estimations of the translation, but parts of the grid were not estimatable due to the pattern doing out of frame.

%	\begin{figure}[h]
%		\centering
%		\includegraphics[width=\linewidth]{imgs/baslerCalibTop}
%		\caption{Second test iteration with smaller distance between calibration pattern and dynamic camera. Note that some points in the top right were cut off due to an out of view pattern.}
%		\label{fig:closertrans}
%	\end{figure}

%	\begin{figure}[h]
%		\centering
%		\includegraphics[width=\linewidth]{imgs/goproCalibTop}
%		\caption{Measured pose vs commanded pose}
%		\label{fig:gptop}
%	\end{figure}

%	\begin{figure}[h]
%		\centering
%		\includegraphics[width=\linewidth]{imgs/goproCalibSide}
%		\caption{Measured pose vs commanded pose with vertical drift}
%		\label{fig:gpside}
%	\end{figure}


	\begin{thebibliography}{9}

		\bibitem{zhuang}
			Hanqi Zhuang; Roth, Zvi S.; Sudhakar, R., \emph{Simultaneous robot/world and tool/flange calibration by solving homogeneous transformation equations of the form AX=YB}, Robotics and Automation, IEEE Transactions on , vol.10, no.4, pp.549,554, Aug 1994.

		\bibitem{arun}
			Arun, K.S.; Huang, T.S.; Blostein, S.D., \emph{Least-Squares Fitting of Two 3-D Point Sets}, Pattern Analysis and Machine Intelligence, IEEE Transactions on , vol.PAMI-9, no.5, pp.698,700, Sept. 1987.



	\end{thebibliography}



\end{document};
