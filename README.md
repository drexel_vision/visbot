MatCam Sampling Robot
=====================

Overview
--------

![alt text](doc/overview.png "System Overview")


This is a ROS package for interfacing for the robot. Where the robot is the

- Under `src/arm` is a node for using the Cyton arm
	- It consists of two nodes:
		- `arm_node` 64bit interface through a ros topics. This requires the `cyton` node to be running.
			- See `arm_node.cpp`
		- `cyton` 32bit hardware driver that interfaces with `arm_node`
			- See `cyton.cpp`
			- The direct arm control is in `cytonarm.cpp`
			- This part connects to the 64bit via a raw TCP socket which is defined in `server.cpp` and `client.cpp`
	- Example code for using it can be found under `scripts/arm`
		-  The important one is `scripts/arm/arm_client.py` which is a wrapper class for connecting to the arm and moving it
		- `scripts/arm/arm_grid.py` shows how to move the arm in a grid and take pictures

- Under `src/camera` contains a node `camera_node` for saves images from the Basler camera
	- Example Python usage is `stc/camera/example.py`


Useful Scripts
--------------

- `scripts/keyboard_ctrl.py` is a keyboard control for the base wheeled platform



Installation
------------

- Testing Platform
	- ArchLinux, 4.0.1-1
		- Development multilib packages also need to be installed
	- NVIDIA 349.16-2, bbswitch 0.8.3.
	- ROS Indigo

- Install all the requirements listed below
- Put this entire folder into your ROS/catkin workspace
- Run `catkin_make` in the root of the workspace


Requirements
------------

- OpenCV

- PCL

- Python Packages
	- Numpy
	- python-pcl
	- https://pypi.python.org/pypi/Quaternion

- Cyton Viewer
	- The code expects it to be extracted to `ext/Robai/Cyton Gamma 300R2 Viewer_3.3.3.a12de6` (relative to this directory)
	- This should be the 32bit version
	- From the CytonViewer, the first EndEffector should be changed to be of type FrameEndEffector
	- The port should be set to /dev/ttyUSB0
	- If you experience issues, see `doc/cyton-viewer-fix.txt`

- Basler Pylon 4 SDK
	- Should be installed to `/opt/pylon4`
	- If you experience issues, see `doc/pylon-viewer-fix.txt`

- Kinect 2 Drivers
	- libfreenect2 (commit 2a3a948177404a0dca6ed26f71d88abc415e2188)
		- Linux Kernel >3.16 is required for the Kinect USB3 drivers to work
		- Nouevua does not work with the libfreenect2 (use the official NVidia drivers)
	- iai\_kinect2 (commit 42787f9bdd7ac04668f8ff85f3209059fca085d2)




Hand Eye Calibration
--------------------

1. Start `roscore`
2. Start the arm and camera drivers with `roslaunch visbot robot.launch`
3. Change the camera intrinsics in `scripts/handeye/settings.py`
4. Run `./scripts/arm/arm_grid.py ./data/handeye` to collect data from the arm and the camera
	- This will fill `./data/handeye/camera` with color images, and will make `./data/handeye/base.npz` which contains the corresponding arm poses. It is an (N, 4, 4) size array of homogenous pose matrices
5. Using the registration\_viewer in iai\_kinect2 save a high quality (1920x1080) point cloud and place all the saved files into `./data/handeye/cloud`
	- This should include a file named `0000_cloud.pcd` and a file named `0000_color.jpg`

6. Run `./script/handeye/process.py` to PnP estimate the image positions
	- This will create `./data/handeye/world.npz` which contains an (N, 4, 4) size array of homogenous pose matrices

7. Either run `./script/handeye/align.py` (1 1/2 solution) or `./script/handeye/coordinate.py` (full possible erroroneous solution) to solve for the full system
	- This will make another file called `./data/handeye/align.npz` with the X and Y matrices

8. Optionally run `./script/handeye/vis.py` to visualize the resultant data correspondence




Extra Stuff
-----------

### Hand Eye Calibration in Paper Form ###
- See `doc/dv-06052015.pdf`
	- The raw TeX is in `doc/paper.tex`


### Camera Mount ###
- Included in `doc/camera_mount` is a 3d printable mount for attaching the Basler camera to the Cyton Arm
- The whole gripper peice needs to removed and the mount attaches directly to the servo
- No additional screws are necessary: you can take the screws from the original gripper to attach the new mount

