#!/usr/bin/python2

# Keyboard control of the robot platform
# Requires the RosAria ros package to be installed and running

import rospy
import curses

from geometry_msgs.msg import * # Vector3 and twist

rospy.init_node('robot_ctrl', anonymous=True)
pub = rospy.Publisher('/RosAria/cmd_vel', Twist, queue_size=1)


stdscr = curses.initscr()
curses.cbreak()
stdscr.keypad(1)

stdscr.addstr(0,10,"Hit 'q' to quit")
stdscr.refresh()

key = ''
while True:
    key = stdscr.getch()

    if key == ord('q'):
        break


    v = Twist(
        Vector3(0,0,0),
        Vector3(0,0,0)
    )

    dirty = False

    stdscr.refresh()
    if key == curses.KEY_UP:
        v.linear.x = 0.1
        dirty = True
    elif key == curses.KEY_DOWN:
        v.linear.x = -0.1
        dirty = True
    elif key == curses.KEY_RIGHT:
        v.angular.z = -0.2
        dirty = True
    elif key == curses.KEY_LEFT:
        v.angular.z = 0.2
        dirty = True
    elif key == ord('s'):
        dirty = True # Stop

    if dirty:
        pub.publish(v)
        dirty = False

curses.endwin()
