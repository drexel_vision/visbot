#!/usr/bin/python2

# Method for detecting the small cube pattern

import cv2
import numpy as np
import pcl

import sys

# The sizes of the chessboard patterns to find
# Detect the largest patterns first to prevent placement ambiguity
# TODO: More work needs to be done for eliminating rotation ambiguity
boards = [(5,5), (5,4), (4,4)]


# The Kinect's camera matrix
import settings
mtx = settings.KINECT_MTX


# minn is the minimum number of patterns that must be matched for success (default requires that all patterns be matched)
# if cloud is given, then the pattern points are also projected onto a plane in the cloud and the 3d points are returned
def find_pattern_points(img, cloud=None, minn=0):

    cv2.namedWindow("", cv2.WINDOW_NORMAL)

    cornerSet = []
    objptsSet = []

    #blured = cv2.GaussianBlur(img, (11,11), 5)
    #gray = cv2.addWeighted(img, 1.5, blured, -0.5, 0)


    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #gray = cv2.equalizeHist(gray)

	# Find all the boards
	# This works by finding the largest one first, whiting out that board, and finding the next one.
    n = 0
    for i in range(0,len(boards)):
        size = boards[i]

        ret, corners = find_chessboard(gray, size)

        if ret == True:
            hull = cv2.convexHull(corners)
            hull = hull.reshape((hull.shape[0], 2)).astype(np.int32)

            cv2.fillConvexPoly(gray, hull, 255)

            cv2.drawChessboardCorners(img, size, corners, True)
            cv2.drawChessboardCorners(gray, size, corners, True)
            cv2.imshow("", gray)
            cv2.waitKey(0)

            cornerSet.append(corners)

            if cloud != None:
                fil = filter_cloud(corners, cloud)

                o, plane = project_planar_pattern(corners, fil)
                objptsSet.append(o)

            #pcl.save(plane, str(i) + ".pcd")

            n = n + 1
        else:
            break


    success = False
    if minn == 0: # Require all boards
        if n == len(boards):
            success = True
    else: # Success would be at least a few boards were found
        if n >= minn:
            success = True



    if success:
        return True, np.concatenate(cornerSet), np.concatenate(objptsSet) if len(objptsSet) > 0 else None
    else:
        return False, None, None


def find_chessboard(gray, size):
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.001)

    ret, corners = cv2.findChessboardCorners(gray, size, flags=cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_FILTER_QUADS + cv2.CALIB_CB_FAST_CHECK)

    if ret == True:
        cv2.cornerSubPix(gray, corners, (7, 7), (-1,-1), criteria)
    else:
        return False, None



    return True, corners




# Given 2d points in the image, return a 3d cloud filtered to the general area around those points
def filter_cloud(corners, cloud):
    minp = [9999]*3
    maxp = [-9999]*3
    for i in range(0, len(corners)):
        c = corners[i]
        p = cloud.get_point(c[0,0], c[0,1])

        #print(" ".join([str(x) for x in p]) + ";")

        for i in range(0,len(minp)):
            if p[i] > maxp[i]:
                maxp[i] = p[i]

            if p[i] < minp[i]:
                minp[i] = p[i]


    fields = ["x", "y", "z"]

    for i in range(0,len(fields)):
        f = fields[i]

        fil = cloud.make_passthrough_filter()
        fil.set_filter_field_name(f)
        fil.set_filter_limits(minp[i], maxp[i])
        cloud = fil.filter()


    return cloud




# Given 2d image points that lie on a planar pattern on a given point cloud, return the 3d points of it
def project_planar_pattern(points, cloud):
    seg = cloud.make_segmenter()
    seg.set_optimize_coefficients(True)
    seg.set_model_type(pcl.SACMODEL_PLANE)
    seg.set_method_type(pcl.SAC_RANSAC)
    seg.set_distance_threshold (0.01)

    indices, model = seg.segment()


    #print(model)

    plane_normal = np.matrix([[model[0]], [model[1]], [model[2]]])
    #plane_hypot = np.linalg.norm(plane_normal)
    #plane_normal = plane_normal / plane_hypot # Normal vector of the plane

    plane_dist = model[3] # / plane_hypot # Distance of the plane from the origin

    plane_point = -plane_dist * plane_normal # A point on the plane

    #print(str(plane_point[0,0]) + " " + str(plane_point[1,0]) + " " + str(plane_point[2,0])  + ";")

    cx = mtx[0,2]
    cy = mtx[1,2]
    fx = mtx[0,0]
    fy = mtx[1,1]

    # Back-Project ray going from the 2d image point to the 3d plane
    def plane_project(x, y):

        s = np.matrix([ [(x - cx) / fx], [(y - cy) / fy ], [1] ])

        t = np.matrix([ [0], [0], [0] ] )

        kn = np.vdot(plane_point - t, plane_normal)
        kd = np.vdot(s, plane_normal)

        k = kn / kd

        p = t + k*s

        return p


    objpts = []
    for i in range(0,len(points)):
        p = points[i]
        obj = plane_project(p[0,0], p[0,1]).reshape((1,3))

        objpts.append(obj)

    return objpts, cloud.project(indices, model)



if __name__ == '__main__':
    img = cv2.imread(sys.argv[1])
    cloud = pcl.load(sys.argv[2]) if len(sys.argv) > 2 else None

    ret, corners, objpts = find_pattern_points(img, cloud)

    print(ret)
    #print(corners)
    #print(objpts)

    #for i in range(0, len(corners)):
    #    print(" ".join([str(x) for x in corners[i][0,:]]) + ";")

    #for i in range(0, len(objpts)):
    #    print(" ".join([str(x) for x in objpts[i][0,:]]) + ";")


    cv2.namedWindow("", cv2.WINDOW_NORMAL)
    cv2.imshow("", img)
    cv2.waitKey(0)
    cv2.imwrite('cube_out.jpg', img)
