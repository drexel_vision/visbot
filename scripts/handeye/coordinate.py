#!/usr/bin/python2


# Solves for the two system transformtions:
# 1. camera -> gripper
# 2. world -> base
#
# Given
# 1. base -> gripper
# 2. world -> camera


# Implementation of the algorithm presented in:
# Simultaneous Robot/World and Tool/Flange Calibration by Solving Homogeneous Transformation Equations of the Form AX = YB
# Hanqi Zhuang. Zvi S. Roth, and R. Sudhakar

import math
import numpy as np
from numpy.linalg import inv
import cv2

import Quaternion
from Quaternion import Quat



def skew(v):
    return np.matrix([
        [0, -v[2, 0], v[1, 0]],
        [v[2,0], 0, -v[0,0]],
        [-v[1,0], v[0,0], 0]
    ])

# All quaternions are assumed to be in the form [w,x,y,z]

def mat2quat(m):
    return Quat(m).q
    #h = np.eye(4)
    #h[:3, :3] = m[:3, :4]

    #return transformations.quaternion_from_matrix(h, isprecise=True)

def quat2mat(q):
    #q = Quaternion.normalize(q)
    return Quat(q).transform
    #m = transformations.quaternion_matrix(q)
    #return m[:3, :3]



# Get the rotation part of the transformation matrix
def R(m):
    return m[0:3, 0:3]

# Get the translation part of the transformation matrix
def p(m):
    return m[:3, 3:]










# Given m samples
# samples are pairs of 4x4 transformation matrices
# of the form:
# array([
#     [A0, B0],
#     [A1, B1],
#     ...
# ])
#


def form_matrices(samples):

    m = len(samples)

    G = np.empty( (3*m, 6) )
    c = np.empty( (3*m, 1) )

    for i in range(0, m):

        Ai, Bi = samples[i]

        # Extract the scalar and vector components of the rotations in quaternion form
        a0 = mat2quat(R(Ai))[0]
        av = np.matrix([ mat2quat(R(Ai))[1:] ]).T
        b0 = mat2quat(R(Bi))[0]
        bv = np.matrix([ mat2quat(R(Bi))[1:] ]).T


        #print(av)
        #print("--")

        #print( (np.dot(av, np.transpose(av))) / a0)

        Gi = np.empty( (3,6) )
        Gi[:, :3] =  a0*np.eye(3) + skew(av) + (av * np.transpose(av)) / a0
        Gi[:, 3:] = -b0*np.eye(3) + skew(bv) - (av * np.transpose(bv)) / a0

        ci = bv - (b0 / a0) * av #np.empty( (3,1) )

        rows = slice(i*3, i*3+3)
        G[rows, :] = Gi
        c[rows, :] = ci


    return G, c



def solve_matrices(G, c):
    # G is (M,N)
    # U is (M,M)
    # V is (N,N)

    # (U s V) x = c
    # (s V) x = U^T c
    # ()
    #

    a = np.linalg.pinv(G)

    w = np.dot(a, c) # a * c

    #print(w)

    return w



def get_rots(w):
    y0 = (1 + w[3,0]**2 + w[4,0]**2 + w[5,0]**2)**(-1/2) # The sign of this doesn't matter
    yv = y0 * np.transpose( w[3:,0] )
    xv = y0 * np.transpose( w[:3,0] )
    x0 = (1 - xv[0]**2 - xv[1]**2 - xv[2]**2)**(-1/2) # TODO: What is the sign of this

    print(1 - xv[0]**2 - xv[1]**2 - xv[2]**2)

    # TODO: make sure that np.dot in this case does vector dot product
    # x0test = np.dot(av / a0, xv) + (b0/a0)*y0 - np.dot(bv/a0, yv)
    # if x0test < 0:
    #     x0 = x0 * -1

    #x0 = x0 * -1

    # Combine them back into quaternions and return the two results

    qx = [x0] + xv.tolist()
    qy = [y0] + yv.tolist()

    Rx = quat2mat(qx)
    Ry = quat2mat(qy)

    return Rx, Ry




# Now that we have the rotation components (Ry, Rx), we can solve for the translation (ty, tx)
#
# We are solving the following equation with least squared:
#     RAi pX + pAi = Ry pBi + pY
#     RAi pX - pY = Ry pBi - pAi
#
# Knowns: RAi, RBi, pAi, pBi,  Ry, Rx
# Unknowns: pX and pY
#
# A Matrix
#
# x vector:
#    [ pX_x, pX_y, pX_z, pY_x, pY_y, pY_z ]
#
# b vector
#
#
def get_translation(samples, Rx, Ry):

    m = len(samples)

    A = np.empty( (3*m, 6) )
    b = np.empty( (3*m, 1) )

    for i in range(0, m):
        Ai, Bi = samples[i]

        rows = range(i*3, i*3+3)
        A[rows, :3] = R(Ai)
        A[rows, 3:] = -np.eye(3)

        b[rows, :] = np.dot(Ry, p(Bi)) - p(Ai)


    x = np.linalg.lstsq(A,b)[0]


    # Form X and Y
    X, Y = np.eye(4), np.eye(4)
    R(X)[:,:] = Rx[:,:]
    R(Y)[:,:] = Ry[:,:]

    p(X)[:,:] = x[:3, :]
    p(Y)[:,:] = x[3:, :]

    return X, Y











from settings import *

with np.load(DATA_DIR + '/world.npz') as X:
    world_camera = X['camera']

with np.load(DATA_DIR + '/base.npz') as X:
    base_gripper = X['gripper']




samples = []




# Convert them to matrices
for i in range(0, base_gripper.shape[0]):
    if not math.isnan(world_camera[i, 0, 0]):

        b = world_camera[i, :, :].copy()
        b[0, 3] = b[0, 3] + 1
        b[1, 3] = b[1, 3] + 2
        b[2, 3] = b[2, 3] + 3

        samples.append([
            world_camera[i, :, :], # Ai
            base_gripper[i, :, :]  # Bi
        ])



G, c = form_matrices(samples)

#print('G:')
#print(G)

#print('')

#print('c:')
#print(c)

w = solve_matrices(G, c)

Rx, Ry = get_rots(w)

X, Y = get_translation(samples, Rx, Ry)

print('X: (camera->gripper)')
print(X)

print('')

print('Y: (world->base)')
print(Y)


print('')
print('Errors:')

for i in range(0, len(samples)):

    Ai, Bi = samples[i]

    e = np.dot(Ai, X) - np.dot(Y, Bi)

    #print(e)


np.savez(DATA_DIR + "/align.npz", camera_gripper=X, world_base=Y)
