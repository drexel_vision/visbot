#!/usr/bin/python2

# Assuming no rotation from the gripper to the camera, this computes the transformation of baseFrame -> worldFrame

import sys

import numpy as np
import math

import Quaternion
from Quaternion import Quat

# Aligns the arm data with the computed positions
# This takes the translation components of the processed data and finds a rigid transformation to match them

from settings import *

with np.load(DATA_DIR + '/base.npz') as X:
    base_gripper = X['gripper']

with np.load(DATA_DIR + '/world.npz') as X:
    world_camera = X['camera']


npoints = base_gripper.shape[0]

# Extract the 3d points from the transformation matrices in the data
bg_points = np.empty((0, 3))
wc_points = np.empty((0, 3))

for i in range(0, npoints):
    if not math.isnan(world_camera[i, 0, 0]): # A nan matrix in the estimated points indicates that a image was not recognizable
        b = base_gripper[i, :3, 3:].reshape((1,3))
        w = world_camera[i, :3, 3:].reshape((1,3))

        bg_points = np.append(bg_points, b, axis=0)
        wc_points = np.append(wc_points, w, axis=0)




# http://nghiaho.com/?page_id=671
# http://nghiaho.com/uploads/code/rigid_transform_3D.py_
def rigid_transform_3D(A, B):
    assert len(A) == len(B)

    N = A.shape[0]; # total points

    centroid_A = np.mean(A, axis=0)
    centroid_B = np.mean(B, axis=0)

    # centre the points
    AA = A - np.tile(centroid_A, (N, 1))
    BB = B - np.tile(centroid_B, (N, 1))

    # dot is matrix multiplication for array
    H = np.transpose(AA) * BB

    U, S, Vt = np.linalg.svd(H)

    R = Vt.T * U.T

    # special reflection case
    if np.linalg.det(R) < 0:
       print "Reflection detected"
       Vt[2,:] *= -1
       R = Vt.T * U.T


    t = -R*centroid_A.T + centroid_B.T

    return R, t


# Align the translations of the point sets
R, t = rigid_transform_3D(np.mat(bg_points), np.mat(wc_points))




Y = np.eye(4)
Y[:3, :3] = R
Y[:3, 3:] = t

print(Y)

print("")

X = np.eye(4)



# Align the orientations of the point sets

n = 0
c = np.array([0, 0, 0, 0])

for i in range(0, npoints):

    if not math.isnan(world_camera[i, 0, 0]):
        rX = np.dot(
            np.linalg.inv(world_camera[i, :, :]),
            np.dot(
                Y,
                base_gripper[i, :, :]
            )
        )

        Q = Quat(rX[:3, :3])

        q = Q.q
        # TODO: Which element is 'w'
        #if q[3] < 0:
        #    q = -1*q

        c = c + q

        n = n + 1

# Take the average rotation
X[:3, :3] = Quat(Quaternion.normalize(c / n)).transform


print(X)


np.savez(DATA_DIR + "/align.npz", camera_gripper=X, world_base=Y)


