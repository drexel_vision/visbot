import numpy as np

# Common settings for running the scripts

# Change this to where the data was collected
DATA_DIR = "./data/08192015"


# True if the camera is mounted 90degress (counterclockwise)
TURN_IMAGE = True




# Instrinsic Camera matrix for the arm camera
CAMERA_MTX = np.matrix([[
            4.9382326285432237e+03, 0., 1.1891905846448844e+03, 0.,
            4.9382326285432237e+03, 8.9605253961306562e+02, 0., 0., 1.
]]).reshape((3,3))

# Camera distortion coefficients for the arm camera
CAMERA_DIST = np.matrix([[
             1.7088530340356733e-03, -5.0555988208217584e-01,
            -1.1120797720256387e-02, 1.0123289473248554e-02,
            5.2752989438870950e+00
]]).reshape((5,1))


# Instrinsic Camera matrix for the Kinect
# We assume the kinect images are already undistorted
KINECT_MTX = np.matrix([[
            1.0952539015302850e+03, 0., 9.7205293261548809e+02, 0.,
            1.0930405114580878e+03, 5.1144391975331376e+02, 0., 0., 1.
]]).reshape((3,3))



import cube
import plane

# Change this to depending on which pattern is being used
DETECTOR = cube.find_pattern_points
#DETECTOR = plane.find_pattern_points
