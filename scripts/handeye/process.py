#!/usr/bin/python2

# Takes in image frames from the arm camera, the base.npz from 'arm/arm_grid.py', and the point cloud and generates 'world.npz' which contains
# the solved positions in the kinect frame of reference

import cv2
import numpy as np
from numpy.linalg import inv
import pcl
import glob
import re

import math

import sys

from settings import *


# In the case that the image is turned, correct the camera matrix
if TURN_IMAGE:
	fx, fy = CAMERA_MTX[0, 0], CAMERA_MTX[1, 1]
	cx, cy = CAMERA_MTX[0, 2], CAMERA_MTX[1, 2]

	# flip components
	CAMERA_MTX[0, 0] = fy
	CAMERA_MTX[1, 1] = fx
	CAMERA_MTX[0, 2] = cy
	CAMERA_MTX[1, 2] = cx




cloud = pcl.load(DATA_DIR + "/cloud/0000_cloud.pcd")
refimg = cv2.imread(DATA_DIR + "/cloud/0000_color.jpg")
inimgs = sorted(glob.glob(DATA_DIR + "/camera/*.jpg"), key=(lambda x: int(re.search(r'([0-9]+)\.(jpg|bmp)$', x).group(1))))


res, refcorners, objp = DETECTOR(refimg, cloud)

if not res:
    print("Error: could not find pattern in cloud reference image")
    sys.exit(1)

cv2.namedWindow("", cv2.WINDOW_NORMAL)
cv2.imshow("", refimg)
print("Press any key on the image to start")
cv2.waitKey(0)


## Code for the case of the 3d points of the chessboard lying on a perfect uniform grid (good for Kinect debugging)
# Allocate the points for the chessboard
#objp = np.zeros((len(refcorners),3), np.float32)
#objp[:,:2] = np.mgrid[0:6,0:8].T.reshape(-1,2)*0.024

#for i in range(0, len(refcorners)):
#    objp[i,:] = objcorners[i]




with np.load(DATA_DIR + '/base.npz') as X:
    base_gripper = X['gripper']


if len(base_gripper) != len(inimgs):
    print("Mismatching data sizes")
    sys.exit(1)

# Data is a set of rotation matrices
world_camera = np.empty((len(base_gripper), 4, 4))


for i in range(0, len(inimgs)):
    name = inimgs[i]

    img = cv2.imread(name)

	# Rotate the image
    if TURN_IMAGE:
        rows, cols = img.shape[:2]

        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), -90, 1)
        dst = cv2.warpAffine(img, M, (cols,rows))

        img = dst



    # Find the pattern
    res, corners, null = DETECTOR(img)

    if not res:
        print("nan nan nan nan nan nan;")

        nan = float('nan')
        world_camera[i, :, :] = np.matrix([
            [nan, nan, nan, nan],
            [nan, nan, nan, nan],
            [nan, nan, nan, nan],
            [nan, nan, nan, nan]
        ])

        continue


    # Find the rotation and translation vectors.

    o = objp[0:len(corners)] # Only use as many points as where where recovered

    rvecs, tvecs, inliers = cv2.solvePnPRansac(o, corners, CAMERA_MTX, CAMERA_DIST, iterationsCount=4000, reprojectionError=4)
    ret, rvecs, tvecs = cv2.solvePnP(o, corners, CAMERA_MTX, CAMERA_DIST, rvecs, tvecs, True, flags=cv2.ITERATIVE)

    mat = np.eye(4)
    rotMat, jac = cv2.Rodrigues(rvecs)
    mat[:3, :3] = rotMat
    mat[:3, 3:] = tvecs

    world_camera[i, :, :] = inv( mat )


    s = " ".join([str(x) for x in tvecs[:,0]]) + ";"
    print(s)



    # Reproject points and show result

    reproj, jacobian = cv2.projectPoints(objp, rvecs, tvecs, CAMERA_MTX, CAMERA_DIST)

    # Calculate the error for the current image
    err = []
    for i in range(0, len(corners)):
        e = corners[i] - reproj[i]
        err.append(np.linalg.norm(e))

    print('Error: ' + str(np.mean(err)))


    for p in reproj:
        cv2.circle(img, (int(p[0,0]), int(p[0,1])), 5, (0,255,255), -1)

    cv2.namedWindow("", cv2.WINDOW_NORMAL)
    cv2.imshow("", img)
    cv2.waitKey(1000)






np.savez(DATA_DIR + '/world.npz', camera=world_camera)
