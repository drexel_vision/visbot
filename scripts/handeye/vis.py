#!/usr/bin/python2

import numpy as np
from numpy.linalg import inv

from settings import *

with np.load(DATA_DIR + '/base.npz') as X:
    base_gripper = X['gripper']

with np.load(DATA_DIR + '/world.npz') as X:
    world_camera = X['camera']

with np.load(DATA_DIR + '/align.npz') as X:
    world_base = X['world_base']
    camera_gripper = X['camera_gripper']


bpoints = np.empty((0, 4))
wpoints = np.empty((0, 4))

for i in range(0, base_gripper.shape[0]):

    b = np.dot( world_base, base_gripper[i, :, :] )
    w = np.dot( world_camera[i, :, :], camera_gripper )

    b = np.matrix([ [ b[0,3], b[1,3], b[2,3], 1  ]  ]).reshape((4,1))
    w = np.matrix([ [ w[0,3], w[1,3], w[2,3], 1  ]  ]).reshape((4,1))

    b = b.reshape((1,4))
    w = w.reshape((1,4))

    bpoints = np.append(bpoints, b, axis=0)
    wpoints = np.append(wpoints, w, axis=0)






import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d', aspect=1)

#plt.gca().set_aspect('equal', adjustable='box')

bpoints = np.asarray(bpoints)
wpoints = np.asarray(wpoints)

ax.plot(bpoints[:, 0], bpoints[:, 1], bpoints[:, 2], 'b', label="Base to Gripper")
ax.plot(wpoints[:, 0], wpoints[:, 1], wpoints[:, 2], 'r', label="World to Camera")

legend = ax.legend(loc='upper center', shadow=True)

ax.set_xlabel('X')
# ax.set_xlim(-0.1, 0.1)
ax.set_ylabel('Y')
# ax.set_ylim(0, 0.2)
ax.set_zlabel('Z')
ax.set_zlim(-0.1, 0.1)



# Plotting rotation


fig = plt.figure()
ay = fig.add_subplot(111, projection='3d', aspect=1)


legendAdded = False
for i in range(0, base_gripper.shape[0]):

    bRot = np.dot(world_base, base_gripper[i, :, :])[:3, :3]
    wRot = np.dot(world_camera[i, :, :], camera_gripper)[:3, :3]

    ps =[
        np.matrix([[1],[0],[0]]),
        np.matrix([[0],[1],[0]]),
        np.matrix([[0],[0],[1]])
    ]

    for p in ps:
        bP = np.dot(bRot, p)
        wP = np.dot(wRot, p)

        if not legendAdded:
            ay.plot([0, bP[0,0]], [0, bP[1,0]], [0, bP[2,0]], 'b', label="Base to Gripper")
            ay.plot([0, wP[0,0]], [0, wP[1,0]], [0, wP[2,0]], 'r', label="World to Camera")
            legendAdded = True
        else:
            ay.plot([0, bP[0,0]], [0, bP[1,0]], [0, bP[2,0]], 'b')
            ay.plot([0, wP[0,0]], [0, wP[1,0]], [0, wP[2,0]], 'r')


ay.set_xlabel('X')
ay.set_xlim(-1, 1)
ay.set_ylabel('Y')
ay.set_ylim(-1, 1)
ay.set_zlabel('Z')
ay.set_zlim(-1, 1)


legend = ay.legend(loc='upper center', shadow=True)


plt.show()
