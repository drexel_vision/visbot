#!/usr/bin/python2

# Method for detecting the planar pattern


import cv2
import numpy as np


# Returns result, 2d points, 3d points
def find_pattern_points(img, cloud=None):

    size = (6, 8)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.001)

    ret, corners = cv2.findChessboardCorners(gray, size, flags=cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_FILTER_QUADS)

    if ret == True:
        cv2.cornerSubPix(gray, corners, (10, 10), (-1,-1), criteria)
        cv2.drawChessboardCorners(img, size, corners, True)
    else:
        return False, None, None


    objp = np.zeros((len(corners),3), np.float32)
    objp[:,:2] = np.mgrid[0:6,0:8].T.reshape(-1,2)*0.024


    return True, corners, objp
