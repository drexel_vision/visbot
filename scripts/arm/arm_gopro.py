#!/usr/bin/python2

# Runs the arm/camera over a grid pattern, takes pictures, and records the position of the arm at each point into raw_data.npz

#import cv2
import numpy as np
from arm_client import Arm
from goprohero import *
import sys
import time
from math import *


a = Arm()


cfg = GoProHero.config()
cfg['command']['shutter'] = {
    'cmd': 'bacpac/SH',
    'translate': {
        'off': '00',
        'on': '01'
    }
}


camera = GoProHero(password='insert password')

camera.command('mode', 'still')

time.sleep(2)


a.move(0, 0, 0.3)


print("Place camera and press enter to continue")
sys.stdin.readline()

arm_data = []


for i in range(-10, 10):
   for j in range(-7, 6):
       print(str(i) + " " + str(j))
       pos = a.move(i*0.01, 0, j*0.01 + 0.3)
       arm_data.append(pos)

       time.sleep(1)

       camera.command('shutter', 'on')
       time.sleep(1.5)

np.savez('raw_data', arm=arm_data)


