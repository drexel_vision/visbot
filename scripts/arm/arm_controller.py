#!/usr/bin/python

# Manual keyboard controller for the arm


from arm_client import Arm
import curses

a = Arm()

x, y, z = 0, 0, 0.3


stdscr = curses.initscr()
curses.cbreak()
stdscr.keypad(1)

stdscr.addstr(0,10,"Hit 'q' to quit")
stdscr.refresh()

key = ''
while True:
    key = stdscr.getch()

    if key == ord('q'):
        break

    stdscr.refresh()
    if key == curses.KEY_UP:
        x = x + 0.05
    elif key == curses.KEY_DOWN:
        x = x - 0.05
    elif key == curses.KEY_RIGHT:
        y = y + 0.05
    elif key == curses.KEY_LEFT:
        y = y - 0.05

    x = round(x, 2)
    y = round(y, 2)
    z = round(z, 2)


    stdscr.addstr(2, 0, "X")
    stdscr.addstr(2, 10, "Y")
    stdscr.addstr(2, 20, "Z")

    stdscr.addstr(3, 0, str(x))
    stdscr.addstr(3, 10, str(y))
    stdscr.addstr(3, 20, str(z))
    a.move(x, y, z)

curses.endwin()
