#!/usr/bin/python2

# Runs the arm/camera over a grid pattern, takes pictures, and records the position of the arm at each point into raw_data.npz

#import cv2
import numpy as np
from arm_client import *
import sys
import time
from math import *

import rospy
from visbot.srv import *



if len(sys.argv) != 2:
    print("Usage: ./arm_grid output_dir")
    sys.exit(1)


import os
import os.path
from os.path import expanduser
data_dir = os.path.abspath( expanduser(sys.argv[1]) )
img_dir = data_dir + "/camera"
if not os.path.exists(img_dir):
    os.makedirs(img_dir)



a = Arm()


a.move(0, 0.15, 0.4)


print("Place camera and press enter to continue")
sys.stdin.readline()

arm_data = []



rospy.wait_for_service('camera/take_picture')
try:
    take_picture = rospy.ServiceProxy('camera/take_picture', SaveFile)
except rospy.ServiceException, e:
    print "Service call failed: %s"%e



n = 1

yrange = range(-5,5)
for i in range(-10, 10):
    for j in yrange:
        print(str(i) + " " + str(j))

        dx = i * 0.01
        dy = j * 0.01

        pos = a.move(dx, 0.15, 0.4+dy, [3.14, 0, 3.14])
        arm_data.append(pos)
        time.sleep(0.5)
        take_picture(img_dir + "/%04d.jpg" % n)
        n = n + 1

    yrange.reverse()


np.savez(data_dir + '/base', gripper=arm_data)
