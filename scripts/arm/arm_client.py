#!/usr/bin/python

# A client for the cyton arm server

import rospy
from visbot.srv import *
from geometry_msgs.msg import *
import time
from math import *
import numpy as np
import transformations as tf


class Arm:

    def __init__(self):
        rospy.wait_for_service('arm/set_pose')
        self._set_pose = rospy.ServiceProxy('arm/set_pose', SetPose)

    #def zero(self):
    #    return self._send("zero")


    # Given a translation and an orientation (as either euler angles or a quaternion), set the position of the arm and return a rotation matrix representating the final pose
    def move(self, x, y, z, orient=None):

        if orient is None:
            orient = [0, 0, 1, 0] #[0, 0, 0, 1] # w x y z
            #orient = [pi, 0.0, pi]

        # Convert from euler angles
        if len(orient) == 3:
            orient = tf.quaternion_from_euler(orient[0], orient[1], orient[2], 'rzyx')

        res = self._set_pose(Pose(
            Point(x, y, z),
            Quaternion(orient[1], orient[2], orient[3], orient[0]) # x y z w
        ))

        pos = res.pose.position
        quat = res.pose.orientation

        data = ([pos.x, pos.y, pos.z], [quat.w, quat.x, quat.y, quat.z])
        #return data

        return arm_to_matrix(data)







# Changes the arm's coordinate frame to the one used by the Kinect
arm2cloud = np.matrix([
    [1, 0, 0, 0],
    [0, 0, -1, 0],
    [0, 1, 0, 0],
    [0, 0, 0, 1]
])


# Converts the data returned from arm.move() into a matrix with the point cloud axes
def arm_to_matrix(data):
    pos, quat = data

    mat = np.eye(4)
    mat[:3, :3] = tf.quaternion_matrix(quat)[:3, :3]
    mat[:3, 3:] = np.array(pos).reshape((3,1))

    return arm2cloud * mat




def test():
    a = Arm()

    #a.move(0, 0.35, 0.35)

    a.move(0, 0, 0.3)
    print("Place camera and press enter to continue")
    sys.stdin.readline()

    for i in range(0, 180, 5):
        print(i)

        r = 0.15
        t = radians(i)

        a.move(r*cos(t), r*sin(t), 0.3)

        time.sleep(0.25)



if __name__ == '__main__':
    test()
