#include "ros/ros.h"
#include "visbot/SaveFile.h"

using namespace std;



bool grab(visbot::SaveFile::Request  &req, visbot::SaveFile::Response &res){


	return true;
}

int main(int argc, char *argv[]){

	ros::init(argc, argv, "cloud_node");
	ros::NodeHandle n;

	ros::ServiceServer service = n.advertiseService("cloud/save", grab);
	ROS_INFO("Ready to grab point clouds.");
	ros::spin();

	return 0;
}
