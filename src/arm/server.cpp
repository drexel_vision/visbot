#include "server.h"
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>

#include <iostream>

// For listening for connections in a separate thread
void *server_run(void *arg);


server::server(int port, message_recvr mr){
	this->port = port;
	this->receiver = receiver;

	cout << receiver << endl;

	this->running = false;
	this->sock = NULL;
}

server::~server(){
	if(running){
		/* This should force it to stop blocking on accept() */
		running = false;
		close(sock);

		//pthread_join(serv->thread, 0);
		//serv->thread = NULL;
	}
}


bool server::start(){
	/* See if already started */
	if(running) {
		return true;
	}

	/* Set up the socket */
	sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);



	if(sock == -1){
		printf("Could not create socket\n");
		return false;
	}

	#ifdef SO_REUSEPORT
	int reuse = 1;
	if(setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &reuse, sizeof(int)) == -1){
		close(sock);
		printf("Failed to make socket reusable\n");
		return false;
	}
	#endif

	struct sockaddr_in addr_in;
	addr_in.sin_family = AF_INET;
	addr_in.sin_addr.s_addr = INADDR_ANY;
	addr_in.sin_port = htons(port);


	if(bind(sock, (sockaddr*) &addr_in, sizeof(addr_in)) < 0){
		close(sock);
		printf("Failed to bind\n");
		return false;
	}

	if(listen(sock, SOMAXCONN) < 0) {
		close(sock);
		printf("Failed to listen\n");
		return false;
	}

	running = true;

	this->run();

	/* Start a new thread to handle incoming requests */
//	serv->running = true;
//	if(pthread_create(&serv->thread, NULL, server_run, serv)) {
//		serv->thread = NULL;
//		serv->running = false;
//		close(serv->sock);
//		return 1;
//	}

	return true;

}

extern void command_rcvr(vector<string> args, int sock);

void server::run(){
	int conn, r;
	struct sockaddr_in addr;
	socklen_t len = sizeof(struct sockaddr_in);


	char buffer[BUF_SIZE];

	while(running){ /* TODO: Make sure this stops properly  */

		conn = accept(sock, (sockaddr*)&addr, &len);

		if(conn == -1){
			printf("problem!!!\n");
			/* Failed to connect, the socket was probably closed */
			break;
		}



		r = recv(conn, buffer, BUF_SIZE, 0);

		if(r == -1 || r == 0){
			printf("failed!\n");
			close(conn);
			continue;
		}


		// Make sure that the string ends in a null pointer
		buffer[r] = '\0';


		vector<string> args;

		char *tok = spacetok(buffer);
		while(tok != NULL){
			args.push_back(tok);
			tok = spacetok(NULL);
		}



		//this->receiver(args, conn);
		command_rcvr(args, conn);

		close(conn);
	}
}

