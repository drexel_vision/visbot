# File based on the Robai Cyton Sdk examples



#Specify minimum cmake requirements
cmake_minimum_required(VERSION 2.8.12)

if(COMMAND cmake_policy)
    cmake_policy(SET CMP0008 NEW)
endif(COMMAND cmake_policy)

project(quickStartHardwareExample)

# Pull in standard configuration.
include(./CytonBuildDefaults.cmake)

# Pull in rendering requirements
include_directories(${render_inc})
link_directories(${render_lib})


add_executable(arm
	src/arm.cpp
	src/cytonarm.cpp
	src/server.cpp
	src/utils.cpp
)

# Link appropriate libraries
target_link_libraries(
   arm

   ecConvertSimulation
   ecCytonHardwareInterface
   ecSystemSimulation
   ecSimulation
   ecConvertSystem
   ecRendCore
   ecManipulator
   ecGrasping
   ecVisualization
   ecFoundCommon
   ecXmlReaderWriter
   ecXml
   ecFoundCore
   ecGeometry
   ecControl
   ecMatrixUtilities
   ecControlCore
   ecFunction
   ecboost_system-mt
)
if(UNIX)
target_link_libraries(
   arm
   ${toolkits}/../bin/imageSensor.ecp
)
endif(UNIX)

set(CMAKE_BUILD_TYPE Release)
