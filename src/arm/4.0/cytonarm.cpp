#include "cytonarm.h"

#include <math.h>

using namespace cyton;

using namespace std;

EcString DATA_DIR = EcString( getenv("CYTON_BIN") );

EcString SIM_FILE = DATA_DIR + "cyton.ecz";
EcString PLUGIN_FILE = DATA_DIR + "cytonPlugin";
EcString CONFIG_FILE = DATA_DIR + "cytonConfig.xml";


CytonArm::CytonArm(){
	hardware = NULL;
}

CytonArm::~CytonArm(){
	std::cout << "Cleaning up hardware..." << std::endl;

	if(hardware != NULL){
		hardware->shutdown();
		delete hardware;
	}
}



bool CytonArm::loadSimulation(){
		// -----------------------------------------------------------------
	// Step #1 - Loading a simulation from a file.
	// -----------------------------------------------------------------



	EcBoolean success;


	std::cout << "Loading Model:" << SIM_FILE <<"\n";
	// load the simulation from an ECZ file
	success = EcXmlObjectReaderWriter::readFromFile(simulationIn, SIM_FILE);


	// make sure it loaded properly
	if(!success)
	{
		EcWARN("Could not load simulation.\n");
		return false;
	}


	return true;
}

bool CytonArm::load(){

   EcRealVector initHwAngles(numJoints, 0.0);
   EcRealVector jointAngles=initHwAngles;

   //update
   if(!hardware->getJointStates(initHwAngles) ||
      !hardware->waitUntilCommandFinished(waitTimeInMS))
   {
      std::cerr << "Problem getting initial angles.\n";
      return false;
   }

	std::cout << initHwAngles.size();

   std::cout << "Initial State: (";
   for(EcU32 ii=0; ii<initHwAngles.size(); ++ii)
   {
      std::cout << initHwAngles[ii] << ", ";
   }
   std::cout << ")\n";

   // -----------------------------------------------------------------
   // Step #3 - Viewing a stated system.
   // -----------------------------------------------------------------

   // declare a visualizable stated system object
//   EcVisualizableStatedSystem visStatedSystem;

   //update VizStatedSystem from hardware angles
   simulationIn.getVisualizableStatedSystem(visStatedSystem);

   EcPositionStateVector states = visStatedSystem.statedSystem().state().positionStates();

   states[0].setJointPositions(initHwAngles);
   visStatedSystem.statedSystem().setPositionStates(states);

   // instantiate a renderer
//   EcRenderWindow renderer;

   // set the size of the window
   renderer.setWindowSize(800,800);

   // set the system
   if(!renderer.setVisualizableStatedSystem(visStatedSystem))
   {
      return false;
   }

   std::cout << "Rendering.\n";
   // view the system
   renderer.renderScene();

   // pause
   EcSLEEPMS(500);


   // -----------------------------------------------------------------
   // Step #4 - Defining a position-control system.
   // -----------------------------------------------------------------

   // a variable holding the position control system
//   EcPositionControlSystem posContSys;

   // set the position control system from the loaded simualtion
   posContSys=simulationIn.positionControlSystem();

   // set the stated system
   posContSys.setStatedSystem(&visStatedSystem.statedSystem());



	const EcIndividualManipulator& cytonManip = posContSys.statedSystem()->system().manipulators()[0];
   const EcManipulatorLink* gripperlink = cytonManip.linkByIndex(7);













   // -----------------------------------------------------------------
   // Example motion - Coordinated joint control
   // -----------------------------------------------------------------
   std::cout << "Starting coordinated joint control example.\n";

   //switch to the joint control end effector set
   posContSys.setActiveEndEffectorSet(0,EcVelocityController::JOINT_CONTROL_INDEX);

   //get the initial joint state
   EcEndEffectorPlacement initalPlacement = posContSys.actualPlacement(0,0);

   EcXmlRealVector goalJointPos;
   goalJointPos.resize(initalPlacement.data().size());

   goalJointPos[0] = 0;
   goalJointPos[1] = -0.7;
   goalJointPos[2] = 0;
   goalJointPos[3] = -0.7;
   goalJointPos[4] = 0;
   goalJointPos[5] = -1;
   goalJointPos[6] = 0;
   //open the gripper all the way
   goalJointPos[7] = gripperlink->jointActuator().upperLimit();

   EcEndEffectorPlacement desiredPlacement = initalPlacement;

   desiredPlacement.setData(goalJointPos);

   posContSys.setDesiredPlacement(0,0,desiredPlacement);

   // a state to update and render, and an object to hold the
   // final pose
   EcManipulatorSystemState dynamicState;
   EcCoordinateSystemTransformation calculatedFinalPose;

   // execution parameters
   EcReal simTimeStep = posContSys.timeStep();
   EcReal simRunTime = 10.0;
   EcU32 steps = simRunTime/simTimeStep;
   EcBoolean achieved = EcFalse;

   // move to the desired pose, and render every timestep
   for(int ii=0;ii<steps;++ii)
   {
      // get the current time
      EcReal currentTime=simTimeStep*ii;

      // calculate the state at current time
      posContSys.calculateState(currentTime,dynamicState);

      // show the manipulator in this position
      renderer.setState(dynamicState);
      renderer.renderScene();

      // set the hardware
      if(!setHardwareJoints(dynamicState.positionStates()[0].jointPositions()))
      {
         std::cerr << "Problem setting angles for step " << ii << ".\n";
         return false;
      }

      EcSLEEPMS(static_cast<EcU32>(1000*simTimeStep));

      if(posContSys.actualPlacement(0,0).approxEq(posContSys.desiredPlacement(0,0),1e-5) )
      {
         //exit loop
         achieved=EcTrue;
         break;
      }
   }

   // check for accuracy
   if(!achieved)
   {
      EcWARN("Did not converge.\n");
      return false;
   }













/*
	EcU32 ii;


	// declare an error return code
	EcBoolean success;




	// -----------------------------------------------------------------
	// Step #3 - Viewing a stated system.
	// -----------------------------------------------------------------

	//update VizStatedSystem from hardware angles
	simulationIn.getVisualizableStatedSystem(visStatedSystem);

	EcPositionStateVector states = visStatedSystem.statedSystem().state().positionStates();

//	numJoints = states[0].jointPositions().size();

	cout << "Simulation has " << numJoints << " joints" << endl;


	// Initialize the simulation zeroed
	EcRealVector initHwAngles(numJoints, 0);
	EcRealVector jointAngles = initHwAngles;


	states[0].setJointPositions(initHwAngles);
	visStatedSystem.statedSystem().setPositionStates(states);



	// set the size of the window
	renderer.setWindowSize(800,800);

	// set the system
	if(!renderer.setVisualizableStatedSystem(visStatedSystem))
	{
		return false;
	}

	std::cout << "Rendering.\n";
	// view the system
	renderer.renderScene();

	// pause
	EcSLEEPMS(500);


	// -----------------------------------------------------------------
	// Step #4 - Defining a position-control system.
	// -----------------------------------------------------------------


	// set the position control system from the loaded simualtion
	posContSys = simulationIn.positionControlSystem();

	// set the stated system
	posContSys.setStatedSystem(&visStatedSystem.statedSystem());

	const EcIndividualManipulator& cytonManip = posContSys.statedSystem()->system().manipulators()[0];
	const EcManipulatorLink* gripperlink = cytonManip.linkByIndex(7);

*/


	cout << "Manipulators" << endl;

	const EcIndividualManipulatorVector &mSet = posContSys.statedSystem()->system().manipulators();

	for(int i = 0; i < mSet.size(); i++){
		cout << "[" << i << "] " << mSet[i].token() << endl;


		const EcEndEffectorSet &eeSet = simulationIn.endEffectorSet(i);

		for(int j = 0; j < eeSet.size(); j++){
			cout << "    [" << j << "] " << eeSet.endEffectors()[j].token() << endl;
		}
	}

	cout << endl << endl;






	return true;
}

EcU32 waitTimeInMS;

bool CytonArm::connect(){

	// -----------------------------------------------------------------
	// Step #2 - Configuring hardware
	// -----------------------------------------------------------------

	//cyton::hardwareInterface hardware(PLUGIN_FILE, CONFIG_FILE);
	hardware = new cyton::hardwareInterface(PLUGIN_FILE, CONFIG_FILE);



	// get ports
	EcStringVector ports = hardware->availablePorts();
	const EcU32 numPorts = ports.size();
	EcBoolean foundPort = EcFalse;
	if(numPorts)
	{

		for(EcU32 ii=0; ii<numPorts; ++ii)
		{
			hardware->setPort(ports[ii].c_str());
			if(hardware->init())
			{
				foundPort=EcTrue;
				break;
			}
		}

		if(foundPort)
		{
			// This gives a velocity of 0.3 radians per second
			const EcReal deltaTime = 10.0; // 10 seconds

			// This method will set the internal routines to go from one position to
			// another in this much time.  It is a convenience routine in lieu of
			// specifying a joint velocity on a per-joint basis.
			hardware->setVelocityFromDeltaTime(deltaTime);
			// Give a pretty conservative wait time (2x)
			waitTimeInMS = EcU32(2 * deltaTime * 1000);
		}
		else
		{
			// did not find port
			std::cout << "Cound not find valid port.\n";
			return false;
		}
	}

	numJoints = hardware->numJoints();
	if(numJoints)
	{
		std::cout << "Found " << numJoints << " servos.\n";
	}
	else
	{
		std::cerr << "Invalid configuration. No joints available.\n";
		return false;
	}


//	this->sync();


	return true;
}


static std::ostream& operator<<(std::ostream& os, const EcRealVector &vec){
	size_t num = vec.size();
	if(num){
		for(size_t ii = 0; ii < num-1; ++ii){
			os << vec[ii] << ", ";
		}
		os << vec[num-1];
	}
	return os;
}


bool CytonArm::set(double x, double y, double z){
	/*
		Vertical

		yaw: 2.85846
		Pitch: 0.08533
		Roll: -3.13

	*/

	/*
		Horizontal (For GoPro)

		0
		0
		M_PI / 2

	*/


	return set(x, y, z, M_PI, 0.0, M_PI);
}

const EcU32 handIndex = 0;


bool CytonArm::set(double x, double y, double z, double yaw, double pitch, double roll){


	EcU32 ii;


	// end effector selection
	const EcU32 handIndex=0;
	EcU32 eeSetIndex=1;

	//switch to frame ee set
	posContSys.setActiveEndEffectorSet(0, eeSetIndex);

	// get the current offset in system coordinates
	EcCoordinateSystemTransformation initialPose=posContSys.actualPlacement(0, handIndex);

	EcCoordinateSystemTransformation finalPose = initialPose;
	EcOrientation desiredOrient;

	desiredOrient.setFrom321Euler(yaw, pitch, roll);
	finalPose.setOrientation(desiredOrient);
	finalPose.setTranslation(EcVector(x, y, z));
	posContSys.setDesiredPlacement(0, handIndex, finalPose);


	EcManipulatorSystemState dynamicState;
	EcCoordinateSystemTransformation calculatedFinalPose;


	// execution parameters
	EcReal simTimeStep = posContSys.timeStep();
	EcReal simRunTime = 10.0;
	EcU32 steps = simRunTime/simTimeStep;
	EcReal startingTime = posContSys.time();
	EcBoolean achieved = EcFalse;

//	cout << startingTime << endl;

	// move to the desired pose, and render every timestep
	for(ii = 0; ii < steps; ++ii)
	{
		// get the current time
		EcReal currentTime=simTimeStep*ii;

		// calculate the state at current time
		posContSys.calculateState(currentTime+startingTime, dynamicState);

		// show the manipulator in this position
		renderer.setState(dynamicState);
		renderer.renderScene();


		// set the hardware
		if(!setHardwareJoints(dynamicState.positionStates()[0].jointPositions()))
		{
			std::cerr << "Problem setting angles for step " << ii << ".\n";
			return false;
		}

		EcSLEEPMS(static_cast<EcU32>(1000*simTimeStep));

		calculatedFinalPose = posContSys.actualPlacementVector()[0].offsetTransformations()[handIndex];

		if(calculatedFinalPose.approxEq(finalPose,1e-5))
		{
			achieved = EcTrue;
			break;
		}
	}
	if(!achieved)
	{
		EcWARN("Did not converge.\n");
		return false;
	}



	hardware->waitUntilCommandFinished(5000);

}

/*
	Returns a vector containing three parts for translation followed by four parts of the quaternion

*/
std::vector<float> CytonArm::getPosition(){

	EcCoordinateSystemTransformation initialPose = posContSys.actualPlacement(0, handIndex);


	EcVector trans = initialPose.translation();


//	double yaw;
//	double pitch;
//	double roll;

//	EcVector rod;

	EcOrientation orig = initialPose.orientation();
//	orig.get321Euler(yaw, pitch, roll);

//	orig.getRodriguesVector(rod);

	std::vector<float> pos;

	// x y z translation
	pos.push_back(trans[0]);
	pos.push_back(trans[1]);
	pos.push_back(trans[2]);

	// w x y z quaternion
	pos.push_back(orig[0]);
	pos.push_back(orig[1]);
	pos.push_back(orig[2]);
	pos.push_back(orig[3]);


//	pos.push_back(yaw);
//	pos.push_back(pitch);
//	pos.push_back(roll);

	return pos;
}


void CytonArm::zero(){

	//switch to the joint control end effector set
	posContSys.setActiveEndEffectorSet(0, EcVelocityController::JOINT_CONTROL_INDEX);

	//get the initial joint state
	EcEndEffectorPlacement initalPlacement = posContSys.actualPlacement(0,0);

	EcXmlRealVector goalJointPos;
	goalJointPos.resize(initalPlacement.data().size());

	goalJointPos[0] = 0;
	goalJointPos[1] = 0;
	goalJointPos[2] = 0;
	goalJointPos[3] = 0;
	goalJointPos[4] = 0;
	goalJointPos[5] = 0;
	goalJointPos[6] = 0;


	const EcIndividualManipulator& cytonManip = posContSys.statedSystem()->system().manipulators()[0];
	const EcManipulatorLink* gripperlink = cytonManip.linkByIndex(7);

	//hardware->getJointStates(max, MaxAngleInRadians);
	goalJointPos[7] = gripperlink->jointActuator().upperLimit();

	//open the gripper all the way
	//goalJointPos[7] = gripperlink->jointActuator().upperLimit();

	EcEndEffectorPlacement desiredPlacement = initalPlacement;

	desiredPlacement.setData(goalJointPos);

	posContSys.setDesiredPlacement(0,0,desiredPlacement);

	// a state to update and render, and an object to hold the
	// final pose
	EcManipulatorSystemState dynamicState;
	EcCoordinateSystemTransformation calculatedFinalPose;

	// execution parameters
	EcReal simTimeStep = posContSys.timeStep();
	EcReal simRunTime = 10.0;
	EcReal startingTime = posContSys.time();
	EcU32 steps = simRunTime/simTimeStep;
	EcBoolean achieved = EcFalse;

	// move to the desired pose, and render every timestep
	for(int ii = 0; ii < steps; ++ii)
	{
		// get the current time
		EcReal currentTime=simTimeStep*ii;

		// calculate the state at current time
		posContSys.calculateState(startingTime + currentTime, dynamicState);

		// show the manipulator in this position
		renderer.setState(dynamicState);
		renderer.renderScene();

		// set the hardware
		if(!setHardwareJoints(dynamicState.positionStates()[0].jointPositions()))
		{
			std::cerr << "Problem setting angles for step " << ii << ".\n";
			return;
		}

		EcSLEEPMS(static_cast<EcU32>(1000*simTimeStep));

		if(posContSys.actualPlacement(0,0).approxEq(posContSys.desiredPlacement(0,0),1e-5) )
		{
			//exit loop
			achieved=EcTrue;
			break;
		}
	}

	// check for accuracy
	if(!achieved)
	{
		EcWARN("Did not converge.\n");
		return;
	}



	// TODO: Make this into a position control system action
	/*
	EcRealVector initHwAngles(numJoints, 0.0);
	EcRealVector jointAngles=initHwAngles;

	EcXmlRealVector z(numJoints, 0.0);

	this->setHardwareJoints(z);
	*/


	hardware->waitUntilCommandFinished(5000);

}


// TODO: This doesn't seem so work correctly
// TODO: Error checking on get and set
void CytonArm::sync(){

	std::cout << "SYNC" << std::endl;

	EcPositionStateVector states = visStatedSystem.statedSystem().state().positionStates();


	EcXmlRealVector jAngles = states[0].jointPositions();


	//	cout << "State: (";
	for(EcU32 ii = 0; ii < jAngles.size(); ++ii)
	{
		cout << jAngles[ii] << ", ";
	}
	cout << ")\n";


	EcRealVector initHwAngles(9, 0.0);


	//update
	if(!hardware->getJointStates(initHwAngles) ||
	   !hardware->waitUntilCommandFinished(waitTimeInMS))
	{
		std::cerr << "Problem getting initial angles.\n";
		return;
	}

	std::cout << "Initial State: (";
	for(EcU32 ii=0; ii<initHwAngles.size(); ++ii)
	{
		std::cout << initHwAngles[ii] << ", ";
	}
	std::cout << ")\n";



	states[0].setJointPositions(initHwAngles);
	visStatedSystem.statedSystem().setPositionStates(states);

	renderer.setState(visStatedSystem.statedSystem().state());

	renderer.renderScene();



	return;
	/*


	EcPositionStateVector states = visStatedSystem.statedSystem().state().positionStates();

//	cout << states[0].jointPositions().size() << endl;

//	cout << numJoints << endl;

	EcRealVector hwAngles(numJoints, 0.0);


	//update
	if(!getHardwareJoints(hwAngles))
	{
		std::cerr << "Problem getting initial angles.\n";
		return;
	}


	EcXmlRealVector jAngles = states[0].jointPositions();


	//	cout << "State: (";
	for(EcU32 ii = 0; ii < jAngles.size(); ++ii)
	{
		cout << jAngles[ii] << ", ";
	}
	cout << ")\n";


//	cout << "Initial State: (";
	for(EcU32 ii = 0; ii < 6; ++ii)
	{
		cout << hwAngles[ii] << ", ";

		jAngles[ii] = hwAngles[ii];

	}
	cout << ")\n";





		for(EcU32 ii = 0; ii< jAngles.size(); ++ii)
	{
		cout << jAngles[ii] << ", ";
	}
	cout << ")\n";


	states[0].setJointPositions(jAngles);

	visStatedSystem.statedSystem().setPositionStates(states);

*/

/*

	EcEndEffectorPlacement initalPlacement = posContSys.actualPlacement(0,0);

	EcXmlRealVector goalJointPos = hwAngles;
	goalJointPos.resize(initalPlacement.data().size());

	EcEndEffectorPlacement desiredPlacement = initalPlacement;

	posContSys.setActualPlacement(0,0)



	//desiredPlacement.setData(goalJointPos);
*/

//	states[0].setJointPositions(hwAngles);

//	posContSys.setSensedState(0, states[0], 100000*posContSys.timeStep());



//	cout << states[0].token() << endl << states[1].token() << endl;

//	cout << states.size() << endl;

//	posContSys.setStatedSystem(&visStatedSystem.statedSystem());

//	visStatedSystem.statedSystem().setPositionStates(states);

	renderer.setState(visStatedSystem.statedSystem().state());

//	if(!renderer.setVisualizableStatedSystem(visStatedSystem)){
//		cout << "Failed to update system with hardware states" << endl;
//		// Log an error
//		//return false;
//	}

	renderer.renderScene();


}



/* Sets the angles of the hardware joints */
EcBoolean CytonArm::setHardwareJoints(const EcXmlRealVector& jointValues){

//	const EcU32 numJoints = jointValues.size();

	m_JointAngles.resize(numJoints);

	// set the hardware
	for(EcU32 jj = 0; jj < numJoints; ++jj)
	{
//		cout << jointValues[jj] << " ";

		m_JointAngles[jj] = jointValues[jj];
	}

//	cout << endl;


	// TODO: Change this to use the alternative new API
	// Keep the gripper open completely
	//EcRealVector max(numJoints);


//	const EcIndividualManipulator& cytonManip = posContSys.statedSystem()->system().manipulators()[0];
//	const EcManipulatorLink* gripperlink = cytonManip.linkByIndex(7);

	//hardware->getJointStates(max, MaxAngleInRadians);
//	m_JointAngles[7] = gripperlink->jointActuator().upperLimit(); //max[7];



	if(!hardware->setJointCommands(m_JointAngles))
	{
		std::cerr << "Problem setting angles.\n";
		return EcFalse;
	}

	return EcTrue;


}

/* Retrieves the angles of the hardware joints */

EcBoolean CytonArm::getHardwareJoints(EcRealVector& jointValues){
	return hardware->getJointStates(jointValues, JointAngleInRadians) && hardware->waitUntilCommandFinished(waitTimeInMS);
}
