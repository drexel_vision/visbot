#include "cytonarm.h"
#include "server.h"
#include "utils.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;


void command_rcvr(vector<string> args, int sock);

CytonArm arm;
server s(8181, command_rcvr);


int main(int argc, char **argv){

//	float test[] = {-3.134474, 0.037338, -3.104569};

//	actinSE::Orientation o;
//	o.setFrom321Euler(test[0], test[1], test[2]);

//	actinSE::Array3 v;

//	o.getRodriguesVector(v);

//	for(int i = 0; i < 4; i++){
//		cout << o[i] << endl;
//	}

//	return 0;


	// Simple argument parsing


	arm.loadSimulation();

	std::cout << "Connecting..." << std::endl;
	arm.connect();

	std::cout << "Loading..." << std::endl;
	arm.load();


	arm.sync();

	std::cout << "Zeroing..." << std::endl;
	//arm.zero();


	EcSLEEPMS(1000);


	std::cout << "Running server..." << std::endl;

	// Wait for commands
	s.start();


	std::cout << "Done!" << std::endl;

	return 0;
}


char buffer[4096];

void command_rcvr(vector<string> args, int sock){

	cout << endl;

	if(args.size() < 1){

		return;
	}

	string name = args[0];
	string resp = ""; // What to respond with


	if(name == "zero"){
		arm.zero();
	}
	else if(name == "set"){

		vector<double> nums;

		double d;
		for(int i = 1; i < args.size(); i++){
			parse_double(args[i].c_str(), &d);
			nums.push_back(d);
		}

		// Print the input position
		cout << ">> ";
		for(int i = 0; i < nums.size(); i++){
			cout << nums[i] << " ";
		}
		cout << endl;


		if(nums.size() == 3){ // set X Y Z
			arm.set(nums[0], nums[1], nums[2]);
		}
		else if(nums.size() == 6){ // set X Y Z Y P R
			arm.set(nums[0], nums[1], nums[2], nums[3], nums[4], nums[5]);
		}


		usleep(1000*1000*2);


		vector<float> sim = arm.getPosition();
		cout << "-- ";
		for(int i = 0; i < sim.size(); i++){
			cout << sim[i] << " ";
		}
		cout << endl;

		arm.sync();

		vector<float> pos = arm.getPosition();

		sprintf(buffer, "%f %f %f %f %f %f %f", pos[0], pos[1], pos[2], pos[3], pos[4], pos[5], pos[6]);

		// Print the feedback position
		std::cout << "<< " << buffer << std::endl;

		resp = buffer;
	}

	write(sock, resp.c_str(), resp.size() + 1);
}
