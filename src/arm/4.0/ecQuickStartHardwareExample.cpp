//     Copyright (c) 2012 Energid Technologies. All rights reserved. ////
//
// Filename:    ecQuickStartHardwareExample.cpp
//
// Description: Holds the quick-start code described in the Users Guide.
//
// Contents:    class EcQuickStartHardwareExample
//
/////////////////////////////////////////////////////////////////////////
#include "ecQuickStartHardwareExample.h"

#include <controlCore/ecFrameEndEffector.h>
#include <control/ecIncludeControlExpressions.h>
#include <controlCore/ecPointEndEffector.h>
#include <control/ecPosContSystem.h>
#include <control/ecVelContSystem.h>
#include <convertSystem/ecVisualizableStatedSystem.h>
#include <foundCore/ecApplication.h>
#include <foundCore/ecConstants.h>
#include <rendCore/ecRenderWindow.h>
#include <systemSimulation/ecSysSimulation.h>
#include <xmlReaderWriter/ecXmlObjectReaderWriter.h>

#include <hardwareInterface/cytonHardwareInterface.h>

/////////////////////////////////////////////////////////////////////////
// Functions:    Big four and other header functions.
// Description:
/////////////////////////////////////////////////////////////////////////

// constructor
EcQuickStartHardwareExample::EcQuickStartHardwareExample()
{
}

// destructor
EcQuickStartHardwareExample::~EcQuickStartHardwareExample()
{
}

// copy constructor
EcQuickStartHardwareExample::EcQuickStartHardwareExample(const EcQuickStartHardwareExample& orig)
{
}

// assignment operator
EcQuickStartHardwareExample& EcQuickStartHardwareExample::operator=(const EcQuickStartHardwareExample& orig)
{
   return *this;
}

/////////////////////////////////////////////////////////////////////////
//End of header functions
/////////////////////////////////////////////////////////////////////////



EcString DATA_DIR = EcString( getenv("CYTON_BIN") );

EcString SIM_FILE = DATA_DIR + "cyton.ecz";
EcString PLUGIN_FILE = DATA_DIR + "cytonPlugin";
EcString CONFIG_FILE = DATA_DIR + "cytonConfig.xml";



/////////////////////////////////////////////////////////////////////////
// Functions:    run()
// Description:  Executes the quick-start code.
/////////////////////////////////////////////////////////////////////////
void EcQuickStartHardwareExample::run()
{
   EcU32 ii;

   // -----------------------------------------------------------------
   // Step #1 - Loading a simulation from a file.
   // -----------------------------------------------------------------

   // declare an error return code
   EcBoolean success;

   // declare a filename
   EcString filename= SIM_FILE;

   // declare a simulation object
   EcSystemSimulation simulationIn;

   std::cout << "Loading Model:" <<filename <<"\n";
   // load the simulation from an ECZ file
   success = EcXmlObjectReaderWriter::readFromFile(simulationIn, filename);


   // make sure it loaded properly
   if(!success)
   {
      EcWARN("Could not load simulation.\n");
      return;
   }


   // -----------------------------------------------------------------
   // Step #2 - Configuring hardware
   // -----------------------------------------------------------------

   EcU32 waitTimeInMS;
   cyton::hardwareInterface hardware(PLUGIN_FILE, CONFIG_FILE);

/*
   // get ports
   EcStringVector ports = hardware.availablePorts();
   const EcU32 numPorts = ports.size();
   EcBoolean foundPort=EcFalse;
   if(numPorts)
   {

      for(EcU32 ii=0; ii<numPorts; ++ii)
      {
         hardware.setPort(ports[ii].c_str());
         if(hardware.init())
         {
            foundPort=EcTrue;
            break;
         }
      }

      if(foundPort)
      {
         // This gives a velocity of 0.3 radians per second
         const EcReal deltaTime = 10.0; // 10 seconds

         // This method will set the internal routines to go from one position to
         // another in this much time.  It is a convenience routine in lieu of
         // specifying a joint velocity on a per-joint basis.
         hardware.setVelocityFromDeltaTime(deltaTime);
         // Give a pretty conservative wait time (2x)
         waitTimeInMS = EcU32(2 * deltaTime * 1000);
      }
      else
      {
         // did not find port
         std::cout << "Cound not find valid port.\n";
         return;
      }
   }

   const EcU32 numJoints = hardware.numJoints();
   if(numJoints)
   {
      std::cout << "Found "<<numJoints<<" servos.\n";
   }
   else
   {
      std::cerr << "Invalid configuration. No joints available.\n";
      return;
   }

*/
   const EcU32 numJoints = 7;


   EcRealVector initHwAngles(numJoints, 0.0);
   EcRealVector jointAngles=initHwAngles;

/*
   //update
   if(!hardware.getJointStates(initHwAngles) ||
      !hardware.waitUntilCommandFinished(waitTimeInMS))
   {
      std::cerr << "Problem getting initial angles.\n";
      return;
   }

*/

   EcPrint(Debug) << "Initial State: (";
   for(EcU32 ii=0; ii<initHwAngles.size(); ++ii)
   {
      EcPrint(Debug) << initHwAngles[ii] << ", ";
   }
   EcPrint(Debug) << ")\n";



   // -----------------------------------------------------------------
   // Step #3 - Viewing a stated system.
   // -----------------------------------------------------------------

   // declare a visualizable stated system object
   EcVisualizableStatedSystem visStatedSystem;

   //update VizStatedSystem from hardware angles
   simulationIn.getVisualizableStatedSystem(visStatedSystem);

   EcPositionStateVector states = visStatedSystem.statedSystem().state().positionStates();

   states[0].setJointPositions(initHwAngles);
   visStatedSystem.statedSystem().setPositionStates(states);

   // instantiate a renderer
   EcRenderWindow renderer;

   // set the size of the window
   renderer.setWindowSize(800,800);

   // set the system
   if(!renderer.setVisualizableStatedSystem(visStatedSystem))
   {
      return;
   }

   std::cout << "Rendering.\n";
   // view the system
   renderer.renderScene();

   // pause
   EcSLEEPMS(500);


   // -----------------------------------------------------------------
   // Step #4 - Defining a position-control system.
   // -----------------------------------------------------------------

   // a variable holding the position control system
   EcPositionControlSystem posContSys;

   // set the position control system from the loaded simualtion
   posContSys=simulationIn.positionControlSystem();

   // set the stated system
   posContSys.setStatedSystem(&visStatedSystem.statedSystem());

   const EcIndividualManipulator& cytonManip = posContSys.statedSystem()->system().manipulators()[0];
   const EcManipulatorLink* gripperlink = cytonManip.linkByIndex(7);


   // -----------------------------------------------------------------
   // Example motion - Coordinated joint control
   // -----------------------------------------------------------------
   std::cout << "Starting coordinated joint control example.\n";

   //switch to the joint control end effector set
   posContSys.setActiveEndEffectorSet(0,EcVelocityController::JOINT_CONTROL_INDEX);

   //get the initial joint state
   EcEndEffectorPlacement initalPlacement = posContSys.actualPlacement(0,0);

   EcXmlRealVector goalJointPos;
   goalJointPos.resize(initalPlacement.data().size());

   goalJointPos[0] = 0;
   goalJointPos[1] = -0.7;
   goalJointPos[2] = 0;
   goalJointPos[3] = -0.7;
   goalJointPos[4] = 0;
   goalJointPos[5] = -1;
   goalJointPos[6] = 0;
   //open the gripper all the way
   goalJointPos[7] = gripperlink->jointActuator().upperLimit();

   EcEndEffectorPlacement desiredPlacement = initalPlacement;

   desiredPlacement.setData(goalJointPos);

   posContSys.setDesiredPlacement(0,0,desiredPlacement);

   // a state to update and render, and an object to hold the
   // final pose
   EcManipulatorSystemState dynamicState;
   EcCoordinateSystemTransformation calculatedFinalPose;

   // execution parameters
   EcReal simTimeStep = posContSys.timeStep();
   EcReal simRunTime = 10.0;
   EcU32 steps = simRunTime/simTimeStep;
   EcBoolean achieved = EcFalse;

   // move to the desired pose, and render every timestep
   for(ii=0;ii<steps;++ii)
   {
      // get the current time
      EcReal currentTime=simTimeStep*ii;

      // calculate the state at current time
      posContSys.calculateState(currentTime,dynamicState);

      // show the manipulator in this position
      renderer.setState(dynamicState);
      renderer.renderScene();

      // set the hardware
      if(!setHardwareValues(hardware,dynamicState.positionStates()[0].jointPositions()))
      {
         std::cerr << "Problem setting angles for step " << ii << ".\n";
         return;
      }

      EcSLEEPMS(static_cast<EcU32>(1000*simTimeStep));

      if(posContSys.actualPlacement(0,0).approxEq(posContSys.desiredPlacement(0,0),1e-5) )
      {
         //exit loop
         achieved=EcTrue;
         break;
      }
   }

   // check for accuracy
   if(!achieved)
   {
      EcWARN("Did not converge.\n");
      return;
   }

   // -----------------------------------------------------------------
   // Example section #5 - Placing the end effector.
   // -----------------------------------------------------------------
   std::cout << "Starting EE control example.\n";
   // end effector selection
   const EcU32 handIndex=0;
   EcU32 eeSetIndex=1;

   //switch to frame ee set
   posContSys.setActiveEndEffectorSet(0,eeSetIndex);

   // get the current offset in system coordinates
   EcCoordinateSystemTransformation
      initialPose=posContSys.actualPlacement(0,handIndex);

   // set the desired position to be(.15,.15,.1)
   EcCoordinateSystemTransformation finalPose = initialPose;
   EcOrientation desiredOrient;
   desiredOrient.setFrom321Euler(0,0,0);
   finalPose.setOrientation(desiredOrient);
   finalPose.setTranslation(EcVector(.17,.17,0.1));
   posContSys.setDesiredPlacement(0,handIndex,finalPose);

   // execution parameters
   simRunTime = 10.0;
   steps = simRunTime/simTimeStep;
   EcReal startingTime=posContSys.time();
   achieved = EcFalse;

   // move to the desired pose, and render every timestep
   for(ii=0;ii<steps;++ii)
   {
      // get the current time
      EcReal currentTime=simTimeStep*ii;

      // calculate the state at current time
      posContSys.calculateState(currentTime+startingTime,dynamicState);

      // show the manipulator in this position
      renderer.setState(dynamicState);
      renderer.renderScene();

      // set the hardware
      if(!setHardwareValues(hardware,dynamicState.positionStates()[0].jointPositions()))
      {
         std::cerr << "Problem setting angles for step " << ii << ".\n";
         return;
      }

      EcSLEEPMS(static_cast<EcU32>(1000*simTimeStep));

      calculatedFinalPose = posContSys.actualPlacementVector()[0].
         offsetTransformations()[handIndex];

      if(calculatedFinalPose.approxEq(finalPose,1e-5))
      {
         achieved=EcTrue;
         break;
      }
   }
   if(!achieved)
   {
      EcWARN("Did not converge.\n");
      return;
   }

   // -----------------------------------------------------------------
   // Example section #6 - Moving the end effector along a path
   // -----------------------------------------------------------------
   std::cout << "Starting EE Path example.\n";

   //switch to Frame ee set
   eeSetIndex = 1;
   posContSys.setActiveEndEffectorSet(0,eeSetIndex);

   // execution parameters
   simRunTime = 10.0;
   steps = simRunTime/simTimeStep;

   EcReal radius=0.05;
   EcU32  loops=2;
   EcOrientation orient(1,0,0,0);
   startingTime=posContSys.time();

   // move to the desired pose, and render the position every
   // time step
   for(ii=0;ii<steps;++ii)
   {
      // get the current time
      EcReal currentTime=simTimeStep*ii;

      // set the pose
      EcCoordinateSystemTransformation pose;
      pose.setOrientation(orient);
      EcReal angle=Ec2Pi*loops*currentTime/simRunTime;
      EcVector offset=radius*EcVector(cos(angle),sin(angle),.1);
      pose.setTranslation(finalPose.translation()+offset);
      posContSys.setDesiredPlacement(0,handIndex,pose);

      // calculate the state at current time
      posContSys.calculateState(
         currentTime+startingTime,dynamicState);

      // show the manipulator in this position
      renderer.setState(dynamicState);
      renderer.renderScene();

      // set the hardware
      if(!setHardwareValues(hardware,dynamicState.positionStates()[0].jointPositions()))
      {
         std::cerr << "Problem setting angles for step " << ii << ".\n";
         return;
      }

      EcSLEEPMS(static_cast<EcU32>(1000*simTimeStep));
   }



   // -----------------------------------------------------------------
   // Example section #7 - Moving the gripper
   // -----------------------------------------------------------------
   std::cout << "Starting gripper control example.\n";
   // end effector selection
   const EcU32 gripperEEIndex=1;

   //switch to frame ee set
   posContSys.setActiveEndEffectorSet(0,eeSetIndex);

   // get the current offset in system coordinates
   EcCoordinateSystemTransformation initialHandPose=posContSys.actualPlacement(0,handIndex);
   EcCoordinateSystemTransformation initialGripperPose=posContSys.actualPlacement(0,gripperEEIndex);
   // set the desired position to be offset from the current position
   EcCoordinateSystemTransformation finalGripperPose = initialGripperPose;

   //close the gripper
   //the gripper ee uses the z valueto control placement
   finalGripperPose.setTranslationZ(gripperlink->jointActuator().lowerLimit());
   posContSys.setDesiredPlacement(0,gripperEEIndex,finalGripperPose);
   posContSys.setDesiredPlacement(0,handIndex,initialHandPose);

   // execution parameters
   simRunTime = 10.0;
   steps = simRunTime/simTimeStep;
   startingTime=posContSys.time();
   achieved=EcFalse;

   // move to the desired pose, and render every timestep
   for(ii=0;ii<steps;++ii)
   {
      // get the current time
      EcReal currentTime=simTimeStep*ii;

      // calculate the state at current time
      posContSys.calculateState(currentTime+startingTime,dynamicState);

      // show the manipulator in this position
      renderer.setState(dynamicState);
      renderer.renderScene();

      // set the hardware
      if(!setHardwareValues(hardware,dynamicState.positionStates()[0].jointPositions()))
      {
         std::cerr << "Problem setting angles for step " << ii << ".\n";
         return;
      }

      EcSLEEPMS(static_cast<EcU32>(1000*simTimeStep));

      calculatedFinalPose = posContSys.actualPlacementVector()[0].
         offsetTransformations()[gripperEEIndex];
      if(calculatedFinalPose.approxEq(finalGripperPose,1e-5))
      {
         achieved=EcTrue;
         break;
      }
   }
   if(!achieved)
   {
      EcWARN("Did not converge.\n");
      return;
   }

   EcSLEEPMS(2000);

   //open the gripper
   finalGripperPose.setTranslationZ(gripperlink->jointActuator().upperLimit());
   posContSys.setDesiredPlacement(0,gripperEEIndex,finalGripperPose);

   // execution parameters
   simRunTime = 5.0;
   steps = simRunTime/simTimeStep;
   startingTime=posContSys.time();
   achieved=EcFalse;

   // move to the desired pose, and render every timestep
   for(ii=0;ii<steps;++ii)
   {
      // get the current time
      EcReal currentTime=simTimeStep*ii;

      // calculate the state at current time
      posContSys.calculateState(currentTime+startingTime,dynamicState);

      // show the manipulator in this position
      renderer.setState(dynamicState);
      renderer.renderScene();

      // set the hardware
      if(!setHardwareValues(hardware,dynamicState.positionStates()[0].jointPositions()))
      {
         std::cerr << "Problem setting angles for step " << ii << ".\n";
         return;
      }

      EcSLEEPMS(static_cast<EcU32>(1000*simTimeStep));

      calculatedFinalPose = posContSys.actualPlacementVector()[0].
         offsetTransformations()[gripperEEIndex];
      if(calculatedFinalPose.approxEq(finalGripperPose,1e-5))
      {
         achieved = EcTrue;
         break;
      }
   }
   if(!achieved)
   {
      EcWARN("Did not converge.\n");
      return;
   }
   EcPRINT("Successfully completed example.\n");


   //unload the cyton plugin
   hardware.shutdown();
}



/// set joint angles in hardware
EcBoolean EcQuickStartHardwareExample::setHardwareValues
   (
   cyton::hardwareInterface& hardware,
   const EcXmlRealVector& jointValues
   )
{
   const EcU32 numJoints = jointValues.size();

   m_JointAngles.resize(numJoints);

   // set the hardware
   for(EcU32 jj=0;jj<numJoints;++jj)
   {
      m_JointAngles[jj]=jointValues[jj];
   }

   if(!hardware.setJointCommands(m_JointAngles))
   {
      std::cerr << "Problem setting angles.\n";
      return EcFalse;
   }

   return EcTrue;
}
