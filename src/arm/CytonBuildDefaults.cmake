# File based on the Robai Cyton Sdk examples

# Make sure we can find the Actin distribution
get_filename_component(sriptDir ${CMAKE_CURRENT_LIST_FILE} PATH)

#set(toolkits ${sriptDir}/../../../../toolkits)
set(toolkits "${sriptDir}/../../ext/Robai/Cyton Gamma 300R2 Viewer_3.3.3.a12de6/toolkits")


set(src ${toolkits}/examples/src)
set(external ${toolkits}/../external)
set(render_inc ${external}/render_OSS-20140131-gcc4.6-i386/OSG-3.1.1/include ${external}/render_OSS-20140131-gcc4.6-i386/Qt-4.8.3/include)
set(render_lib  ${external}/sensor_OSS-20140131-gcc4.6-i386/OpenCV-2.4.2/lib ${external}/render_OSS-20140131-gcc4.6-i386/OSG-3.1.1/lib ${external}/render_OSS-20140131-gcc4.6-i386/Qt-4.8.3/lib ${external}/sensor_OSS-20140131-gcc4.6-i386/tiff-3.9.5/lib ${external}/sensor_OSS-20140131-gcc4.6-i386/libdc1394-2.1.0/lib ${external}/sensor_OSS-20140131-gcc4.6-i386/libraw1394-2.0.2/lib)
set(qt_moc_path ${external}/render_OSS-20140131-gcc4.6-i386/Qt-4.8.3/include/../bin)

# Set directory to include headers
include_directories(${toolkits}/../include ${external}/boost-20140131-gcc4.6-i386/boost_1_53_0/include)

# Set directory to library files.
link_directories(${toolkits}/../lib)
link_directories(${external}/boost-20140131-gcc4.6-i386/boost_1_53_0/include/../lib)

set(qt_lib QtCore QtGui)
set(osg_lib osg)

add_definitions(-DEC_BUILD_SHARED_LIBS)
add_definitions(-DEC_HAVE_ACTIN)
add_definitions(-DUNICODE)
add_definitions(-D_UNICODE)



#set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")

if (NOT(${CMAKE_SYSTEM_NAME} STREQUAL "Windows"))
	set(qt_lib QtCore QtGui)
	set(osg_lib osg)
else()
	set(qt_lib QtCore4 QtGui4)
endif()



