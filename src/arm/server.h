#ifndef SERVER_H_
#define SERVER_H_

#define BUF_SIZE 4096

#include <vector>
#include <string>

using namespace std;

/* TCP Server for receiving commands */

//typedef void (*message_callback)();
typedef void (*message_recvr)(vector<string> args, int sock);

class server {

public:
	server(int port, message_recvr mr);
	~server();

	/* Starts the server and begins to listen for connections */
	bool start();

private:

	bool running;

	int port;
	int sock;

	message_recvr receiver;

	// accept() loop
	void run();

};




#endif
