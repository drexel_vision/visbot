#ifndef EcQuickStartHardwareExample_H_
#define EcQuickStartHardwareExample_H_
//     Copyright (c) 2012 Energid Technologies. All rights reserved. ////
//
// Filename:    EcQuickStartHardwareExample.h
//
// Description: Holds the hardware quick-start code.
//
// Contents:    class EcQuickStartHardwareExample
//
/////////////////////////////////////////////////////////////////////////

#include <cytonHardwareInterface.h>
#include <xml/ecXmlVectorType.h>

/** Holds the hardware quick-start code.
*/
class EcQuickStartHardwareExample
{
public:
   /// constructor
   EcQuickStartHardwareExample
      (
      );

   /// destructor
   virtual ~EcQuickStartHardwareExample
      (
      );

   /// copy constructor
   EcQuickStartHardwareExample
      (
      const EcQuickStartHardwareExample& orig
      );

   /// assignment operator
   EcQuickStartHardwareExample& operator=
      (
      const EcQuickStartHardwareExample& orig
      );

   /// run the quick-start code
   virtual void run
      (
      );

   /// set joint angles in hardware
   virtual EcBoolean setHardwareValues
      (
      cyton::hardwareInterface& hardware,
      const EcXmlRealVector& jointValues
      );

   mutable EcRealVector m_JointAngles;
};

#endif // EcQuickStartHardwareExample_H_
