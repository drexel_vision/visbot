#ifndef CYTONARM_H_
#define CYTONARM_H_


#include <cytonHardwareInterface.h>

#include <control/ecFrameEndEffector.h>
#include <control/ecIncludeControlExpressions.h>
#include <control/ecPointEndEffector.h>
#include <control/ecPosContSystem.h>
#include <control/ecVelContSystem.h>
#include <convertSystem/ecVisualizableStatedSystem.h>
#include <foundCore/ecApplication.h>
#include <foundCore/ecConstants.h>
#include <rendCore/ecRenderWindow.h>
#include <simulation/ecSysSimulation.h>

#include <xml/ecXmlVectorType.h>
#include <xml/ecXmlObjectReaderWriter.h>




// New requirements for ControlSystem

#include <actinSE/ControlSystem.h>
#include <actinSE/EndEffector.h>
#include <actinSE/CoordinateSystemTransformation.h>
#include <cytonHardwareInterface.h>


class CytonArm{
public:

	CytonArm();
	~CytonArm();

	bool load();

	/* Connect the hardware driver to a serial port */
	bool connect();


	/* Set the position of the arm to the initial position */
	void zero();

	/* Sets the simulation to the current position of the hardware */
	void sync();

	bool set(double x, double y, double z);

	bool set(double x, double y, double z, double yaw, double pitch, double roll);


	std::vector<float> getPosition();

	void test();


private:


	//Joint angles and Rate vector
	EcRealVector jointAngles, jointRates;

	//ControlSystem object
	actinSE::ControlSystem control;


	/* The current */
	EcRealVector m_JointAngles;
	EcCoordinateSystemTransformation m_Pose;
	EcU32 numJoints;

	EcU32 waitTimeInMS;


	EcBoolean setHardwareJoints(const EcXmlRealVector& jointValues);
	EcBoolean getHardwareJoints(EcRealVector& jointValues);

	cyton::hardwareInterface *hardware = NULL;
};








#endif

