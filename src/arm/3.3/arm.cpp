#include "cytonarm.h"
#include "server.h"
#include "utils.h"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

void command_rcvr(vector<string> args, int sock);

CytonArm arm;
server s(8181, command_rcvr);





int main(int argc, char **argv){


	// Simple argument parsing


	std::cout << "Loading..." << std::endl;
	arm.load();



	std::cout << "Connecting..." << std::endl;
	arm.connect();

	// TODO: This method needs to be fixed
	//arm->sync();

	//arm.display();


	std::cout << "Zeroing..." << std::endl;
//	arm.zero();

//	arm.test();

	EcSLEEPMS(1000);


	std::cout << "Running server..." << std::endl;

	// Wait for commands
	s.start();

	//arm->test();

	std::cout << "Done!" << std::endl;

	return 0;
}


char buffer[4096];

void command_rcvr(vector<string> args, int sock){

	if(args.size() < 1){

		return;
	}

	string name = args[0];
	string resp = ""; // What to respond with


	if(name == "zero"){
		arm.zero();
	}
	else if(name == "set"){

		vector<double> nums;

		double d;
		for(int i = 1; i < args.size(); i++){
			parse_double(args[i].c_str(), &d);
			nums.push_back(d);
		}

		if(nums.size() == 3){ // set X Y Z
			arm.set(nums[0], nums[1], nums[2]);
		}
		else if(nums.size() == 6){ // set X Y Z Y P R
			arm.set(nums[0], nums[1], nums[2], nums[3], nums[4], nums[5]);
		}

		usleep(1000*1000*2);

		vector<float> pos = arm.getPosition();

		sprintf(buffer, "%f %f %f %f %f %f", pos[0], pos[1], pos[2], pos[3], pos[4], pos[5]);

		std::cout << buffer << std::endl;

		resp = buffer;
	}

	write(sock, resp.c_str(), resp.size() + 1);
}
