#include "cytonarm.h"

#include <math.h>

using namespace cyton;
using namespace actinSE;

CytonArm::CytonArm(){

}

CytonArm::~CytonArm(){
	std::cout << "Cleaning up hardware..." << std::endl;
	if(this->hardware != NULL){
		delete hardware;
	}
}


EcString DATA_DIR = EcString( getenv("CYTON_BIN") );

EcString SIM_FILE = DATA_DIR + "cyton.ecz";
EcString PLUGIN_FILE = DATA_DIR + "cytonPlugin";
EcString CONFIG_FILE = DATA_DIR + "cytonConfig.xml";



/// Pull the starting position.
EndEffectorVector eeVec;

bool CytonArm::load(){
	if(!control.loadFromFile(SIM_FILE)){
		std::cout << "Failed to load ControlSystem" << std::endl;
		return false;
	}


//	control.getParam<ControlSystem::NumJoints>(numJoints);;


	// TODO: Change the dimension of these
	jointAngles.clear();
	jointRates.clear();

	// Make sure that the simulation is zeroed
//	std::cout << "Initial state of hardware = " << jointAngles << std::endl;
	EcBoolean passed = control.setParam<ControlSystem::JointAngle>(jointAngles);

	/// Will return EcFalse if not linked with rendering version of lib.
	EcBoolean Rendering = control.setParam<ControlSystem::Rendering>(EcTrue);





	if(!control.getParam<ControlSystem::EndEffectors>(eeVec))
	{
		std::cout << "Problem getting EE vector.\n";
		//ROS_INFO("Problem getting EE vector.\n");
		return EcFalse;
	}

	for(int i = 0; i < eeVec.size(); i++){
		std::cout << i << " " << eeVec[i].name() << std::endl;
	}


}

EcBoolean CytonArm::connect(){

	// TODO: Make these two files have absolute paths
	this->hardware = new cyton::hardwareInterface(PLUGIN_FILE, CONFIG_FILE);
//	hardware->setInitAndShutdownMode(InitFromHardware | TorqueDisable);


	// get ports
	EcStringVector ports = hardware->availablePorts();
	const EcU32 numPorts = ports.size();
	EcBoolean foundPort = EcFalse;
	if(numPorts){

		std::cout << "Tested ports:\n";
		for(EcU32 ii = 0; ii < numPorts; ++ii){
			std::cout << "\t" << ports[ii] << "\n";
			hardware->setPort(ports[ii].c_str());
			if(hardware->init()){
				foundPort=EcTrue;
				break;
			}
		}

		if(foundPort){

			std::cout << "Found port" << std::endl;

/*
			// This gives a velocity of 0.3 radians per second
			const EcReal deltaTime = 5.0; // 10 seconds

			// This method will set the internal routines to go from one position to
			// another in this much time.  It is a convenience routine in lieu of
			// specifying a joint velocity on a per-joint basis.
			hardware->setVelocityFromDeltaTime(deltaTime);
			// Give a pretty conservative wait time (2x)
			waitTimeInMS = EcU32(2 * deltaTime * 1000);
*/
		}
		else{
			std::cout << "Cound not find valid port.\n";
			return false;
		}
	}

	numJoints = hardware->numJoints();
	if(numJoints){
		std::cout << "Found "<< numJoints <<" joints.\n";
	}
	else{
		std::cerr << "Invalid configuration. No joints available.\n";
		return false;
	}

	return true;
}


static std::ostream& operator<<(std::ostream& os, const EcRealVector &vec){
	size_t num = vec.size();
	if(num){
		for(size_t ii = 0; ii < num-1; ++ii){
			os << vec[ii] << ", ";
		}
		os << vec[num-1];
	}
	return os;
}


EcBoolean CytonArm::set(double x, double y, double z){
	/*
		Vertical

		yaw: 2.85846
		Pitch: 0.08533
		Roll: -3.13

	*/

	/*
		Horizontal (For GoPro)

		0
		0
		M_PI / 2

	*/



	return set(x, y, z, M_PI, 0.0, M_PI);
}

EcReal currentTime = 0.0;

EcBoolean CytonArm::set(double x, double y, double z, double yaw, double pitch, double roll){

	EndEffector &ee = eeVec[0];

	///clearing jointAngles and Rates
	jointAngles.clear();
	jointRates.clear();


	CoordinateSystemTransformation startPos;
	ee.getParam<EndEffector::ActualPose>(startPos);

	Array3 startTrans = startPos.translation();

//	std::cout << "Starting EE position: " << startTrans << std::endl;

	double startYaw;
	double startPitch;
	double startRoll;

	Orientation orig = startPos.orientation();
	orig.get321Euler(startYaw, startPitch, startRoll);

//	std::cout << startYaw << " " << startPitch << " " << startRoll << std::endl;


//	std::cout << "::" << currentTime << std::endl;


	EcBoolean passed = true;


//	EcReal currentTime = 0.0; // time counter for this pass

//	control.getParam<ControlSystem::SimulationTime>(currentTime);

	///Recieving EE points from action client
	Array3Vector point;
	std::vector<Orientation> orients;


//	point.push_back(Array3(x, y, z));

//	point.push_back(Array3( 0, 0.35, 0.35 ));

//	float one = 1.0 * (M_PI / 180.0);
//	float angle = one;

	Array3 targetTrans(x, y, z);
	double targetYaw = yaw; //0.0; //-1.0*M_PI;
	double targetPitch = pitch; // 0.0;
	double targetRoll = roll; //M_PI / 2.0;


	/*
	for(int i = 0; i <= 10; i++){
		double a = i / 10.0;

		point.push_back(Array3(
			targetTrans[0]*a + startTrans[0]*(1.0-a),
			targetTrans[1]*a + startTrans[1]*(1.0-a),
			targetTrans[2]*a + startTrans[2]*(1.0-a)
		));

		Orientation o;
		o.setFrom321Euler(
			targetYaw*a + startYaw*(1.0-a),
			targetPitch*a + startPitch*(1.0-a),
			targetRoll*a + startRoll*(1.0-a)

		);

		orients.push_back(o);
	}
	*/

	point.push_back(targetTrans);
	Orientation o;
	o.setFrom321Euler(
		targetYaw,
		targetPitch,
		targetRoll

	);

	orients.push_back(o);


	EcRealVector max(numJoints);
	hardware->getJointStates(max, MaxAngleInRadians);


	for(size_t ii = 0; passed && ii < point.size(); ++ii)
	{

//	Yaw: 0.141958
//	Pitch: 0.141958
//	Roll: -1.33726

		Array3 p = point[ii];
		Orientation o = orients[ii];

		std::cout << ">>>" << std::endl;
		std::cout << p << " " << yaw << " " << pitch << " " << roll << std::endl;

//		float yaw = 0.0; //-2.0*sin(p[0]); //0.0; //0.0;
//		float pitch =  0.0; //((float)ii) * one;
//		float roll = -1*(M_PI / 2.0); //-1.33726; //0.0;

//		Orientation orient;
//		orient.setFrom321Euler(yaw, pitch, roll); //Give yaw,pitch,roll values

		CoordinateSystemTransformation coord;
		coord.setOrientation(o);
		coord.setTranslation(p);

//		std::cout << "Point #" << ii << " of " << point.size() << std::endl;


		passed &= ee.setParam<EndEffector::DesiredPose>(coord);
		//std::cout << "Desired EE position: " << point[ii] << std::endl;

		const EcReal totalTime = currentTime + 5; // Give 20s for each point
		const EcReal timeStep = 0.1;   // 10Hz update


		CoordinateSystemTransformation actualPos = startPos;

		/// Run until we get where we want to go, or it took too long.
		while(passed && !actualPos.approxEq(coord, 1e-4) && currentTime <= totalTime)
		{
			// Calculate a new state at the given time.
			passed &= control.calculateToNewTime(currentTime);

			// Pull updated joint angles after calculating.
			passed &= control.getParam<ControlSystem::JointAngle>(jointAngles);
			passed &= control.getParam<ControlSystem::JointVelocity>(jointRates);
			// Pass them directly to hardware

			// Get EE position for loop check.
			passed &= ee.getParam<EndEffector::ActualPose>(actualPos);



			jointAngles[numJoints - 1] = max[numJoints - 1];


			// Set from jointAngles and jointRates
			hardware->setJointCommands(jointAngles, jointRates);

			// Calculate our current time.
			currentTime += timeStep;

			if(currentTime > totalTime){
				std::cout << "Timeout" << std::endl;
			}

			ee.getParam<EndEffector::ActualPose>(actualPos);

			EcSLEEPMS(100);
			// TODO: Preempty checking

		}
		//If passed return false

		if(!passed){
			std::cout << "Fail" << std::endl;

			return EcFalse;
		}
		//Passing result position to /cyton/result topic
		else{
			// return actualPos
			//return EcTrue;
		}

	}

	hardware->waitUntilCommandFinished(5000);


	return EcTrue;
}

std::vector<float> CytonArm::getPosition(){

	EcRealVector hwAngles(numJoints);
	this->getHardwareJoints(hwAngles);
	control.setParam<ControlSystem::JointAngle>(hwAngles);

	EndEffector &ee = eeVec[0];

	CoordinateSystemTransformation startPos;
	ee.getParam<EndEffector::ActualPose>(startPos);

	Array3 trans = startPos.translation();


	double yaw;
	double pitch;
	double roll;

	Orientation orig = startPos.orientation();
	orig.get321Euler(yaw, pitch, roll);

	std::vector<float> pos;

	pos.push_back(trans[0]);
	pos.push_back(trans[1]);
	pos.push_back(trans[2]);
	pos.push_back(yaw);
	pos.push_back(pitch);
	pos.push_back(roll);

	return pos;
}


void CytonArm::zero(){

	std::cout << numJoints << std::endl;
	EcRealVector initAngles(numJoints, 0.0);


	EcRealVector max(numJoints);
	hardware->getJointStates(max, MaxAngleInRadians);
	initAngles[numJoints-1] = max[numJoints - 1];


	// Set all joints to their init value.
	std::cout << "Setting all joints to init(0) angles.\n";
	for(int i = 0; i < 50; i++){
		hardware->setJointCommands(initAngles);
		EcSLEEPMS(100);
	}

	/*
	if(!hardware->setJointCommands(initAngles) ||
	   !hardware->waitUntilCommandFinished( 5000 )) // waitTimeInMS
	{
		std::cerr << "Problem setting initialize angles.\n";
		return;
	}
	*/
}


// TODO: This doesn't seem so work correctly
// TODO: Error checking on get and set
void CytonArm::sync(){


	std::cout << numJoints << std::endl;
	EcRealVector joints(numJoints);

	std::cout << joints << std::endl;

	getHardwareJoints(joints);

	control.setParam<ControlSystem::JointAngle>(joints);
}


/* Runs the arm simulation through extrema points to see the reach of the arm */
void CytonArm::test(){

	/*
	EcRealVector angles { 0.0, -1.25, -0.28, 1.38, -0.56, 1.41, -1.28, 0.0 };

	hardware->setJointCommands(angles);
	control.setParam<ControlSystem::JointAngle>(angles);

	control.calculateToNewTime(0);
	control.calculateToNewTime(10);
	*/

	/*
	EcRealVector max(numJoints);
	EcRealVector min(numJoints);

	hardware->getJointStates(max, MaxAngleInRadians);
	hardware->getJointStates(min, MinAngleInRadians);



	EcRealVector angles;
	control.getParam<ControlSystem::JointAngle>(angles);


	angles[1] = max[1];

	control.setParam<ControlSystem::JointAngle>(angles);






	EndEffectorVector eeVec;
	control.getParam<ControlSystem::EndEffectors>(eeVec);

	EndEffector& ee = eeVec[0];

	Array3 actualPos;
	ee.getParam<EndEffector::ActualPose>(actualPos);

	std::cout << actualPos << std::endl;

	hardware->setJointCommands(angles);
	*/

}


/* Sets the angles of the hardware joints */
EcBoolean CytonArm::setHardwareJoints(const EcXmlRealVector& jointValues){
	numJoints = jointValues.size();

	m_JointAngles.resize(numJoints);

	// set the hardware
	for(EcU32 jj=0;jj<numJoints;++jj){
		m_JointAngles[jj]=jointValues[jj];
	}

	EcRealVector max(numJoints);
	hardware->getJointStates(max, MaxAngleInRadians);
	m_JointAngles[7] = max[7];

	if(!hardware->setJointCommands(m_JointAngles)){
		std::cerr << "Problem setting angles.\n";
		return EcFalse;
	}

	return EcTrue;
}

/* Retrieves the angles of the hardware joints */

EcBoolean CytonArm::getHardwareJoints(EcRealVector& jointValues){
	return hardware->getJointStates(jointValues, JointAngleInRadians);
}
