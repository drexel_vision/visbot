// Take 321 Eulers and spit out as quaternions



// New requirements for ControlSystem

#include <actinSE/ControlSystem.h>
#include <actinSE/EndEffector.h>
#include <actinSE/CoordinateSystemTransformation.h>


#include <iostream>

using namespace std;


using namespace actinSE;


int main(int argc, char *argv[]){


	float test[] = {-3.030429,  0.025937, -3.097835};

	Orientation o;
	o.setFrom321Euler(test[0], test[1], test[2]);

	Array3 v;

	o.getRodriguesVector(v);

	for(int i = 0; i < 4; i++){
		cout << o[i] << endl;
	}

	return 0;


}
