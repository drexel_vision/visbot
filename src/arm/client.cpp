#include "client.h"

#include "utils.h"

#include <sys/socket.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>



client::client(char *address, int port){
	this->address = address;
	this->port = port;

}


vector<string> client::send_message(char *msg){

	int sock = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in addr;

	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_port = htons(this->port);
	addr.sin_family = AF_INET;
	inet_pton(AF_INET, this->address, &addr.sin_addr);

	int r = connect(sock, (struct sockaddr *)&addr, sizeof(struct sockaddr_in));

	// Send message
	r = send(sock, msg, strlen(msg) + 1, NULL);


	char buffer[1024];

	// Wait for a response
	r = recv(sock, buffer, sizeof(buffer), 0);

	vector<string> args;

	char *tok = spacetok(buffer);
	while(tok != NULL){
		args.push_back(tok);
		tok = spacetok(NULL);
	}



	/* TODO: Eventually have persistent sockets to servers for frequently used connections */
	close(sock);

	return args;
}
