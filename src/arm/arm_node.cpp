#include "ros/ros.h"
#include "visbot/SetPose.h"
#include "client.h"

using namespace std;


client cmd_client("127.0.0.1", 8181);


bool set_pose(visbot::SetPose::Request &req, visbot::SetPose::Response &res){

	char buf[256];
	sprintf(buf, "set %f %f %f %f %f %f %f",
			req.pose.position.x, req.pose.position.y, req.pose.position.z,
			req.pose.orientation.w, req.pose.orientation.x, req.pose.orientation.y, req.pose.orientation.z
	);


	vector<string> rawres = cmd_client.send_message(buf);
	vector<float> nums;

	for(int i = 0; i < 7; i++){
		nums.push_back(atof(rawres[i].c_str()));
	}


	printf("%f %f %f\n", nums[0], nums[1], nums[2]);

	res.pose.position.x = nums[0];
	res.pose.position.y = nums[1];
	res.pose.position.z = nums[2];
	res.pose.orientation.w = nums[3];
	res.pose.orientation.x = nums[4];
	res.pose.orientation.y = nums[5];
	res.pose.orientation.z = nums[6];

	return true;
}



int main(int argc, char *argv[])
{
	ros::init(argc, argv, "arm_node");
	ros::NodeHandle n;


	ros::ServiceServer service = n.advertiseService("arm/set_pose", set_pose);
	ROS_INFO("Arm Node Ready");
	ros::spin();


	return 0;
}
