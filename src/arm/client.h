#ifndef CLIENT_H_
#define CLIENT_H_

#include <vector>
#include <string>

using namespace std;



class client {


public:
	client(char *address, int port);

	vector<string> send_message(char *msg);

private:

	char *address;
	int port;

};






#endif

