#include "ros/ros.h"
#include "visbot/SaveFile.h"

#include <pylon/PylonIncludes.h>
using namespace Pylon;
using namespace GenApi;

using namespace std;

CInstantCamera *Camera;


bool grab(visbot::SaveFile::Request  &req, visbot::SaveFile::Response &res){

	CGrabResultPtr ptrGrabResult;

	Camera->GrabOne( 1000, ptrGrabResult);

	if(ptrGrabResult && ptrGrabResult->GrabSucceeded()){

		CPylonImage img;

		img.AttachGrabResultBuffer(ptrGrabResult);

		// ImageFileFormat_Tiff
		// ImageFileFormat_Png
		img.Save(ImageFileFormat_Jpeg, req.filename.c_str());

		img.Release();
	}

	return true;
}

int main(int argc, char *argv[]){

	Pylon::PylonAutoInitTerm autoInitTerm;


	Camera = new CInstantCamera( CTlFactory::GetInstance().CreateFirstDevice());


	ros::init(argc, argv, "camera_server");
	ros::NodeHandle n;

	ros::ServiceServer service = n.advertiseService("camera/take_picture", grab);
	ROS_INFO("Ready to take pictures.");
	ros::spin();

	return 0;
}
