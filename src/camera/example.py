#!/usr/bin/python2

import sys
import rospy
from visbot.srv import *



rospy.wait_for_service('camera/take_picture')
try:
    take_picture = rospy.ServiceProxy('camera/take_picture', SaveFile)
    take_picture("test.jpg")
except rospy.ServiceException, e:
    print "Service call failed: %s" % e
